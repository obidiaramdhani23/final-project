package com.example.secondhand.viewmodel

import com.example.secondhand.datastoreviewmodelmodule
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.module.Module
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import kotlin.test.assertEquals

class DataStoreViewModelTest: KoinTest{
    private lateinit var mockModule: Module
    private val datastoreviewmodel: DataStoreViewModel by inject()

    @Before
    fun setUp() {
        mockModule = module {
            single { datastoreviewmodelmodule }
        }
        startKoin { modules(mockModule) }
    }

    @After
    fun after(){
        stopKoin()
    }

    @Test
    fun test() = runBlocking{
        datastoreviewmodel.saveAccesToken("AccessToken")
        println(datastoreviewmodel.getAccessToken())
        assertEquals("AccessToken", datastoreviewmodel.getAccessToken().value)
    }
}