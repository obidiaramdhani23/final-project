package com.example.secondhand.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.secondhand.model.GetNotificationResponse
import com.example.secondhand.model.GetNotificationResponseItem
import com.example.secondhand.model.PatchNotificationResponse
import com.example.secondhand.network.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationViewModel(private val Api: ApiService): ViewModel() {
    var getNotifData = MutableLiveData<List<GetNotificationResponseItem>>()
    var getNotifCode: Int? = null

    var patchNotifData = MutableLiveData<PatchNotificationResponse>()
    var patchNotifCode: Int? = null

    fun getNotification(token:String) {
        Api.getNotification(token)
            .enqueue(object : Callback<GetNotificationResponse> {
                override fun onResponse(
                    call: Call<GetNotificationResponse>,
                    response: Response<GetNotificationResponse>
                ) {
                    getNotifData.postValue(response.body())
                    getNotifCode = response.code()
                    Log.d("listnotif", response.body().toString())
                }

                override fun onFailure(call: Call<GetNotificationResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }
            })
    }

    fun patchNotification(token:String, id:Int) {
        Api.patchNotification(token, id)
            .enqueue(object : Callback<PatchNotificationResponse> {
                override fun onResponse(
                    call: Call<PatchNotificationResponse>,
                    response: Response<PatchNotificationResponse>
                ) {
                    patchNotifData.postValue(response.body())
                    patchNotifCode = response.code()
                    Log.d("patchnotif", response.body().toString())
                }

                override fun onFailure(call: Call<PatchNotificationResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }
            })
    }
}