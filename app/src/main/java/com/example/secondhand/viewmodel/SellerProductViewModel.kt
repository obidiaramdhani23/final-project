package com.example.secondhand.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.secondhand.apiModule
import com.example.secondhand.model.DeleteResponse
import com.example.secondhand.model.GetSellerProductResponse
import com.example.secondhand.model.GetSellerProductResponseItem
import com.example.secondhand.model.PatchSellerProduct
import com.example.secondhand.model.PostSellerProductResponse
import com.example.secondhand.network.ApiService
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class SellerProductViewModel(private val Api: ApiService): ViewModel() {
    var postProductResponse = MutableLiveData<PostSellerProductResponse>()
    var postProductCode = MutableLiveData<Int?>()

    var getProductResponse = MutableLiveData<List<GetSellerProductResponseItem>>()
    var getProductCode : Int?=null

    var deleteProductResponse = MutableLiveData<DeleteResponse>()
    var deleteProductCode = MutableLiveData<Int?>()

    var patchProductResponse = MutableLiveData<PatchSellerProduct>()
    var patchProductCode = MutableLiveData<Int?>()

    fun postProduct(
        access_token:String,
        name:RequestBody,
        description:RequestBody,
        base_price:RequestBody,
        category_ids:RequestBody,
        location:RequestBody,
        image:MultipartBody.Part) {
        Api.postSellerProduct(access_token, name, description, base_price, category_ids, location, image)
            .enqueue(object : Callback<PostSellerProductResponse> {
                override fun onResponse(
                    call: Call<PostSellerProductResponse>,
                    response: Response<PostSellerProductResponse>
                ) {
                    postProductResponse.postValue(response.body())
                    postProductCode.postValue(response.code())
                }

                override fun onFailure(call: Call<PostSellerProductResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }
            })
    }

    fun deleteProduct(access_token: String, id: Int){
        Api.deleteProduct(access_token, id)
            .enqueue(object : Callback<DeleteResponse>{
                override fun onResponse(
                    call: Call<DeleteResponse>,
                    response: Response<DeleteResponse>
                ) {
                    deleteProductResponse.postValue(response.body())
                    deleteProductCode.postValue(response.code())
                }

                override fun onFailure(call: Call<DeleteResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }

            })
    }

    fun putProduct(
        access_token:String,
        id: Int,
        name:RequestBody,
        description:RequestBody,
        base_price:RequestBody,
        category_ids:RequestBody,
        location:RequestBody,
        image:MultipartBody.Part) {
        Api.putSellerProduct(access_token,id, name, description, base_price, category_ids, location, image)
            .enqueue(object : Callback<PostSellerProductResponse> {
                override fun onResponse(
                    call: Call<PostSellerProductResponse>,
                    response: Response<PostSellerProductResponse>
                ) {
                    postProductResponse.postValue(response.body())
                    postProductCode.postValue(response.code())
//                    Log.d("productresponse", response.errorBody()!!.string())
                }

                override fun onFailure(call: Call<PostSellerProductResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }
            })
    }



    fun getProduct(
        access_token: String
    ){
        Api.getSellerProduct(access_token)
            .enqueue(object : Callback<GetSellerProductResponse>{
                override fun onResponse(
                    call: Call<GetSellerProductResponse>,
                    response: Response<GetSellerProductResponse>
                ) {
                    getProductResponse.postValue(response.body())
                    getProductCode= response.code()
                }

                override fun onFailure(call: Call<GetSellerProductResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }

            })
    }

    fun getSellerProductById(
        access_token: String, id: Int
    ){
        Api.getSellerProductById(access_token,id)
            .enqueue(object : Callback<GetSellerProductResponse>{
                override fun onResponse(
                    call: Call<GetSellerProductResponse>,
                    response: Response<GetSellerProductResponse>
                ) {
                    getProductResponse.postValue(response.body())
                    getProductCode= response.code()
                }

                override fun onFailure(call: Call<GetSellerProductResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }

            })
    }

    fun patchProduct(
        access_token:String,
        id:Int,
        status:RequestBody) {
        Api.patchSellerProduct(access_token, id, status)
            .enqueue(object : Callback<PatchSellerProduct> {
                override fun onResponse(
                    call: Call<PatchSellerProduct>,
                    response: Response<PatchSellerProduct>
                ) {
                    patchProductResponse.postValue(response.body())
                    patchProductCode.postValue(response.code())
                }

                override fun onFailure(call: Call<PatchSellerProduct>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }
            })
    }
}