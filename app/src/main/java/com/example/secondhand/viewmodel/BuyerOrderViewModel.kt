package com.example.secondhand.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.secondhand.model.GetBuyerOrderResponse
import com.example.secondhand.model.GetBuyerOrderResponseItem
import com.example.secondhand.model.OrderRequest
import com.example.secondhand.model.PostBuyerOrderResponse
import com.example.secondhand.network.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BuyerOrderViewModel(private val Api: ApiService): ViewModel() {
    var postBuyerOrder = MutableLiveData<PostBuyerOrderResponse>()
    var postBuyerOrderCode= MutableLiveData<Int?>()
    var getBuyerOrder = MutableLiveData<List<GetBuyerOrderResponseItem>>()
    var getBuyerOrderCode: Int? = null

    fun postBuyerOrder(token: String, request: OrderRequest){
        Api.postBuyerOrder(token, request)
            .enqueue(object :Callback<PostBuyerOrderResponse>{
                override fun onResponse(
                    call: Call<PostBuyerOrderResponse>,
                    response: Response<PostBuyerOrderResponse>
                ) {
                    postBuyerOrder.postValue(response.body())
                    postBuyerOrderCode.postValue(response.code())
                }

                override fun onFailure(call: Call<PostBuyerOrderResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure",t)
                }

            })
    }
    fun getBuyerOrder(token: String){
        Api.getBuyerOrder(token)
            .enqueue(object :Callback<GetBuyerOrderResponse>{
                override fun onResponse(
                    call: Call<GetBuyerOrderResponse>,
                    response: Response<GetBuyerOrderResponse>
                ) {
                    if (response.isSuccessful){
                        getBuyerOrder.postValue(response.body())
                        getBuyerOrderCode = response.code()
                    }
                }

                override fun onFailure(call: Call<GetBuyerOrderResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure",t)
                }

            })
    }
}