package com.example.secondhand.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.secondhand.model.GetCategoryResponse
import com.example.secondhand.model.GetCategoryResponseItem
import com.example.secondhand.network.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CategoryViewModel(private val Api: ApiService): ViewModel() {
    var getCategoryData = MutableLiveData<List<GetCategoryResponseItem>>()
    var getCategoryCode: Int? = null
    var ids = MutableLiveData<ArrayList<Int>>()

    fun getCategory() {
        Api.getCategoryy()
            .enqueue(object : Callback<GetCategoryResponse> {
                override fun onResponse(
                    call: Call<GetCategoryResponse>,
                    response: Response<GetCategoryResponse>
                ) {
                    getCategoryData.postValue(response.body())
                    getCategoryCode = response.code()
                    Log.d("listkategori", response.body().toString())
                }

                override fun onFailure(call: Call<GetCategoryResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }
            })
    }
}