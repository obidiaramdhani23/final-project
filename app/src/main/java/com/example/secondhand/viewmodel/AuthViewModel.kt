package com.example.secondhand.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.secondhand.model.EditUserRequest
import com.example.secondhand.model.*
import com.example.secondhand.model.LoginRequest
import com.example.secondhand.model.PostLoginResponse
import com.example.secondhand.model.PostRegisterResponse
import com.example.secondhand.model.RegisterRequest
import com.example.secondhand.network.ApiService
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AuthViewModel(private val Api: ApiService): ViewModel() {
    var postLoginData = MutableLiveData<PostLoginResponse>()
    var postLoginCode: Int? = null

    var postRegisterData = MutableLiveData<PostRegisterResponse>()
    var postRegisterCode: Int? = null

    var userResponseData = MutableLiveData<UserResponse>()
    var userResponseCode:Int?=null

    var editUserData = MutableLiveData<EditUserResponse>()
    var editUserCode: Int? = null

    fun postlogin(request: LoginRequest) {
        Api.postLogin(request)
            .enqueue(object : Callback<PostLoginResponse> {
                override fun onResponse(
                    call: Call<PostLoginResponse>,
                    response: Response<PostLoginResponse>
                ) {
                    postLoginData.postValue(response.body())
                    postLoginCode = response.code()
                }

                override fun onFailure(call: Call<PostLoginResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }
            })
    }

    fun postregister(request: RegisterRequest) {
        Api.postRegister(request)
            .enqueue(object : Callback<PostRegisterResponse> {
                override fun onResponse(
                    call: Call<PostRegisterResponse>,
                    response: Response<PostRegisterResponse>
                ) {
                    postRegisterData.postValue(response.body())
                    postRegisterCode = response.code()
                }

                override fun onFailure(call: Call<PostRegisterResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }
            })
    }

    fun getLogin(token:String){
        Api.getUser(token).enqueue(object : Callback<UserResponse>{
            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                userResponseData.postValue(response.body())
                userResponseCode = response.code()
                Log.d("getlogin", response.body().toString())
            }

            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                Log.e("Repository", "onFailure", t)
            }

        })
    }

    fun getEditUser(token:String,
                    full_name: RequestBody,
                    city:RequestBody,
                    address:RequestBody,
                    phone_number:RequestBody,
                    image:MultipartBody.Part){
        Api.getEditUser(token, full_name, city, address, phone_number, image)
            .enqueue(object : Callback<EditUserResponse>{
                override fun onResponse(
                    call: Call<EditUserResponse>,
                    response: Response<EditUserResponse>
                ) {
                    editUserData.postValue(response.body())
                    editUserCode = response.code()
                    Log.d("edit user", response.body().toString())
                    Log.d("edit user", response.errorBody().toString())
                }

                override fun onFailure(call: Call<EditUserResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }

            })
    }


}