package com.example.secondhand.viewmodel

import android.util.Log
import androidx.datastore.preferences.protobuf.Api
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.secondhand.model.PatchSellerOrder
import com.example.secondhand.model.PatchSellerProduct
import com.example.secondhand.network.ApiService
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SellerOrderViewModel(private val Api: ApiService): ViewModel() {
    var patchOrderResponse = MutableLiveData<PatchSellerOrder>()
    var patchOrderCode = MutableLiveData<Int?>()

    fun patchOrder(
        access_token:String,
        id:Int,
        status: RequestBody
    ) {
        Api.patchSellerOrder(access_token, id, status)
            .enqueue(object : Callback<PatchSellerOrder> {
                override fun onResponse(
                    call: Call<PatchSellerOrder>,
                    response: Response<PatchSellerOrder>
                ) {
                    patchOrderResponse.postValue(response.body())
                    patchOrderCode.postValue(response.code())
                }

                override fun onFailure(call: Call<PatchSellerOrder>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }
            })
    }
}