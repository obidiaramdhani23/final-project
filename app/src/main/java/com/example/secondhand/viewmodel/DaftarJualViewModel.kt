package com.example.secondhand.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.secondhand.model.GetSellerOrder
import com.example.secondhand.model.GetSellerOrderItem
import com.example.secondhand.network.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DaftarJualViewModel(private val Api : ApiService): ViewModel() {
    var getSellerOrderData = MutableLiveData<List<GetSellerOrderItem>?>()
    var getSellerOrderCode: Int? = null

    fun getSellerOrder(token: String, status: String){
        Api.getSellerOrder(token, status)
            .enqueue(object : Callback<GetSellerOrder>{
                override fun onResponse(
                    call: Call<GetSellerOrder>,
                    response: Response<GetSellerOrder>
                ) {
                    getSellerOrderData.postValue(response.body())
                    getSellerOrderCode = response.code()
                    Log.d("orderdata",response.body().toString())
                }

                override fun onFailure(call: Call<GetSellerOrder>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }

            })
    }

}