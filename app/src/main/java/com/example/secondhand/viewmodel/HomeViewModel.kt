package com.example.secondhand.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import com.example.secondhand.repository.Repository

class HomeViewModel(private val repository: Repository): ViewModel() {

    fun fetchAllProduct(page:Int, per_page: Int, status: String, context: Context?)= repository.fetchAllProduct(page,per_page, status, context)
//    fun getProductData() = repository.getAllProduct()
    fun fetchAllCategory(context: Context?) = repository.fetchListCategory(context)
    fun fetchProductByCategoriesId(category_Id: Int,page: Int,per_page: Int, context: Context?)= repository.fetchProductByCategoriesId(category_Id,page, per_page, context)
    fun fetchUserData(access_token: String, userId: Int, context: Context?) = repository.fetchUserData(access_token, userId, context)
    fun fetchProductBySearch(search: String?, context: Context?) = repository.fetchProductBySearch(search, context)
    fun fetchBanner(context: Context?) = repository.fetchAllBanner(context)
    fun fetchProductById(id: Int, context: Context?) = repository.fetchProductById(id, context)
}