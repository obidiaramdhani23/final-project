package com.example.secondhand.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.secondhand.manager.DataStoreManager
import com.example.secondhand.model.GetUserData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class DataStoreViewModel(datastore:DataStoreManager): ViewModel() {
    private val datastore = datastore
    fun saveAccesToken(token:String) {
        viewModelScope.launch {
            datastore.setAccessToken(token)
        }
    }

    fun getAccessToken() : LiveData<String> {
        return datastore.getAccessToken().asLiveData()
    }

    fun saveUserData(token:GetUserData) {
        viewModelScope.launch {
            datastore.setUserData(token)
        }
    }

    fun getUserData() : LiveData<GetUserData> {
        return datastore.getUserData().asLiveData()
    }
}