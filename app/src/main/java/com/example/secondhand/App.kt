package com.example.secondhand

import android.app.Application
import com.example.secondhand.manager.DataStoreManager
import com.example.secondhand.network.ApiService
import com.example.secondhand.repository.Repository
import com.example.secondhand.room.ProductDb
import com.example.secondhand.viewmodel.*
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf(
                authViewModelModule,
                repositoryModule,
                homeViewModel,
                apiModule,
                retrofitModule,
                datastoreviewmodelmodule,
                categoryviewmodel,
                sellerproductviewmodel,
                buyerOrderViewModel,
                notifikasiviewmodel,
                daftarjualviewmodel,
                sellerOrderViewModel
            ))
        }
    }
}


val authViewModelModule = module {
    single {
        AuthViewModel(get())
    }
}
val repositoryModule = module {
    single { Repository(get()) }
}

val homeViewModel = module {
    viewModel { HomeViewModel(get()) }
}
val apiModule = module {
    fun provideUseApi(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    single { provideUseApi(get()) }
}

val retrofitModule = module {
    fun provieHttpClient(): OkHttpClient {
        val client = OkHttpClient.Builder()
        return client.build()
    }

    fun provideRetrofit(client: OkHttpClient): Retrofit{
        return Retrofit.Builder()
            .baseUrl("https://market-final-project.herokuapp.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    single { provieHttpClient() }
    single { provideRetrofit(get()) }
}

val datastoreviewmodelmodule = module {
    single {
        DataStoreManager(androidContext())
    }

    single {
        DataStoreViewModel(get())
    }
}

val categoryviewmodel = module {
    single {
        CategoryViewModel(get())
    }
}

val sellerproductviewmodel = module {
    single {
        SellerProductViewModel(get())
    }
}

val notifikasiviewmodel = module {
    single {
        NotificationViewModel(get())
    }
}

val buyerOrderViewModel = module {
    viewModel { BuyerOrderViewModel(get()) }
}

val daftarjualviewmodel = module{
    viewModel { DaftarJualViewModel(get()) }
}

val sellerOrderViewModel = module {
    viewModel { SellerOrderViewModel(get()) }
}

