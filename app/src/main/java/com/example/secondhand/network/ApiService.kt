package com.example.secondhand.network


import com.example.secondhand.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

open interface ApiService {
    @POST("auth/login")
    fun postLogin(
        @Body request:LoginRequest
    ): Call<PostLoginResponse>

    @POST("auth/register")
    fun postRegister(
        @Body request:RegisterRequest
    ): Call<PostRegisterResponse>

    @GET("seller/category")
    fun getCategoryy(): Call<GetCategoryResponse>

    @Multipart
    @POST("seller/product")
    fun postSellerProduct(
        @Header ("access_token") access_token:String,
        @Part ("name") name:RequestBody,
        @Part ("description") description:RequestBody,
        @Part ("base_price") base_price:RequestBody,
        @Part ("category_ids") category_ids:RequestBody,
        @Part ("location") location:RequestBody,
        @Part image:MultipartBody.Part
    ): Call<PostSellerProductResponse>

    @Multipart
    @PATCH("seller/product/{id}")
    fun patchSellerProduct(
        @Header("access_token") token:String,
        @Path("id") id:Int,
        @Part ("status") status:RequestBody
    ): Call<PatchSellerProduct>

    @Multipart
    @PATCH("seller/order/{id}")
    fun patchSellerOrder(
        @Header("access_token") token:String,
        @Path("id") id:Int,
        @Part ("status") status:RequestBody
    ): Call<PatchSellerOrder>

    @PUT("seller/product/{id}")
    fun putSellerProduct(
        @Header ("access_token") access_token:String,
        @Path ("id") id: Int,
        @Part ("name") name:RequestBody,
        @Part ("description") description:RequestBody,
        @Part ("base_price") base_price:RequestBody,
        @Part ("category_ids") category_ids:RequestBody,
        @Part ("location") location:RequestBody,
        @Part image:MultipartBody.Part
    ): Call<PostSellerProductResponse>

    @GET("auth/user")
    fun getUser(@Header("access_token") token:String):Call<UserResponse>

    @Multipart
    @PUT("auth/user")
    fun getEditUser(
        @Header("access_token") token:String,
        @Part ("full_name")full_name:RequestBody,
        @Part ("city")city:RequestBody,
        @Part ("address")address:RequestBody,
        @Part ("phone_number")phone_number:RequestBody,
        @Part image: MultipartBody.Part
    ):Call<EditUserResponse>



    @GET("buyer/product")
    fun getProduct(
        @Query("page") page: Int,
        @Query("per_page") per_page: Int,
        @Query("status") status: String,
//        @Query("category_id") category_id: Int
    ): Call<GetProductResponse>

    @GET("buyer/product")
    fun getProductbyCategoriesId(
//        @Query("status") status: String = "available",
        @Query("category_id") categoryId: Int,
        @Query("page") page: Int,
        @Query("per_page") per_page: Int
    ): Call<GetProductResponse>

    @GET("buyer/product")
    fun getProductbySearch(
//        @Query("status") status: String = "available",
//        @Query("category_id") categoryId: Int
        @Query("search") search: String?
    ): Call<GetProductResponse>

    @GET("buyer/product/{id}")
    fun getProductById(
        @Path("id") id : Int
    ): Call<GetProductByIdResponse>


    @GET("seller/category")
    fun getCategory(): Call<GetCategories>

    @GET("auth/user")
    fun getUser(
        @Header("access_token") access_token: String,
        @Query("userId") user_id: Int
    ): Call<GetUserData>

    @GET("notification")
    fun getNotification(
        @Header("access_token") token:String
    ): Call<GetNotificationResponse>

    @GET("notification")
    fun getNotificationbyStatus(
        @Header("access_token") token:String,
        @Query("status") status: String
    ): Call<GetNotificationResponse>

    @PATCH("notification/{id}")
    fun patchNotification(
        @Header("access_token") token:String,
        @Path("id") id:Int
    ): Call<PatchNotificationResponse>

    @GET("notification")
    fun getNotificationBuyer(
        @Header("access_token") token: String,
        @Query("buyer") buyer: String
    ): Call<GetNotificationResponse>

    @GET("seller/banner")
    fun getBanner(): Call<GetBannerHomepage>

    @POST("buyer/order")
    fun postBuyerOrder(
        @Header("access_token") token: String?,
        @Body request: OrderRequest
    ): Call<PostBuyerOrderResponse>

    @GET("buyer/order")
    fun getBuyerOrder(
        @Header("access_token")token: String?
    ): Call<GetBuyerOrderResponse>

    @GET("seller/product")
    fun getSellerProduct(
        @Header("access_token")token: String?
    ): Call<GetSellerProductResponse>

    @GET("seller/product/{id}")
    fun getSellerProductById(
        @Header("access_token")token: String?,
        @Path("id")id:Int?
    ): Call<GetSellerProductResponse>

    @GET("seller/order")
    fun getSellerOrder(
        @Header("access_token")token: String?,
        @Query("status")status: String?
    ): Call<GetSellerOrder>

    @DELETE("seller/product/{id}")
    fun deleteProduct(
        @Header("access_token")token: String?,
        @Path("id") id: Int
    ): Call<DeleteResponse>
}