package com.example.secondhand.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.example.secondhand.R
import com.example.secondhand.model.GetSellerProductResponseItem

class MenuResult1() : RecyclerView.Adapter<MenuResult1.MyHolder>() {
    private var mList = ArrayList<GetSellerProductResponseItem>()
    private var onItemClickCallback: MenuResult1.OnItemClickCallback?=null

    interface OnItemClickCallback {
        fun onItemClicked(data: GetSellerProductResponseItem)
    }
    fun setOnItemClickCallback(onItemClickCallback: MenuResult1.OnItemClickCallback){
        this.onItemClickCallback = onItemClickCallback
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }
    inner class MyHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val images = itemView.findViewById<ImageView>(R.id.ivPosterProduct)
        val title = itemView.findViewById<TextView>(R.id.tvTitleProduct)
        val categoriesSeller = itemView.findViewById<TextView>(R.id.tvCategoryTitle)
        val priceProduct = itemView.findViewById<TextView>(R.id.tvPrice)
        val cardViewAdd = itemView.findViewById<CardView>(R.id.cardView2)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.category_item, parent, false)
        return MyHolder(layout)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        val data = mList[position]
        holder.itemView.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                onItemClickCallback?.onItemClicked(data)
            }
        })
        Glide.with(holder.itemView)
            .load(data.imageUrl)
            .transform(CenterCrop())
            .into(holder.images)
        holder.title.setText(data.name)
        holder.categoriesSeller.setText(data.categories.elementAtOrNull(0)?.name)
        holder.priceProduct.setText(data.basePrice.toString())

//        if (data.id == 0){
//            if (position == 0){
//                holder.cardViewAdd.visibility = View.VISIBLE
//            }else{
//                holder.cardViewAdd.visibility = View.GONE
//                holder.itemView.visibility = View.GONE
//            }
//        }else if (data.id > 0){
//            if (position == 0){
//                holder.cardViewAdd.visibility = View.VISIBLE
//            }
//            holder.cardViewAdd.visibility = View.GONE
//            holder.itemView.visibility = View.VISIBLE
//        }else if(data.id == null){
//            holder.itemView.visibility = View.GONE
//        }
        when(data.id){
            0 -> {
                holder.cardViewAdd.visibility = View.VISIBLE
            }
            else -> {
                holder.cardViewAdd.visibility = View.GONE
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }
    @SuppressLint("NotifyDataSetChanged")
    fun setData(mList: ArrayList<GetSellerProductResponseItem>){
        this.mList = mList
        notifyDataSetChanged()
    }
}