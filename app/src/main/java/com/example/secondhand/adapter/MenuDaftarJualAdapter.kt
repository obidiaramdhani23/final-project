package com.example.secondhand.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.secondhand.R
import com.example.secondhand.model.MenuItemDaftarJual

class MenuDaftarJualAdapter(private var mList: List<MenuItemDaftarJual>,
                            private val context: Context
) : RecyclerView.Adapter<MenuDaftarJualAdapter.MyHolder>() {
    var row_index = -1
    private var onItemClickCallback: MenuDaftarJualAdapter.OnItemClickCallBack?=null

    interface OnItemClickCallBack {
        fun onItemClicked(data : MenuItemDaftarJual)
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallBack){
        this.onItemClickCallback = onItemClickCallback
    }

    inner class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.findViewById<TextView>(R.id.tvCategoryTitle)
        val cardView = itemView.findViewById<CardView>(R.id.carcCategory)
        val rvMenuResult1 = itemView.findViewById<RecyclerView>(R.id.rvMenuResult1)
        val rvMenuResult2 = itemView.findViewById<RecyclerView>(R.id.rvMenuResult2)
        val rvMenuResult3 = itemView.findViewById<RecyclerView>(R.id.rvMenuResult3)
         fun bind(data : MenuItemDaftarJual){
             cardView.setOnClickListener{ onItemClickCallback?.onItemClicked(data)}
         }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        return MyHolder(view)
    }

    override fun onBindViewHolder(holder: MyHolder, @SuppressLint("RecyclerView") position: Int) {
        val itemViewModel = mList[position]
        holder.name.text = itemViewModel.title
        holder.cardView.setOnClickListener(object : View.OnClickListener{
            @SuppressLint("NotifyDataSetChanged")
            override fun onClick(view: View) {
                row_index = position
                onItemClickCallback?.onItemClicked(itemViewModel)
                notifyDataSetChanged()
            }
        })
        if (row_index==position){
            holder.bind(itemViewModel)
            holder.cardView.setCardBackgroundColor(context.resources.getColor(R.color.purple_500))
            holder.name.setTextColor(context.resources.getColor(R.color.white))
        }else {
            holder.cardView.setCardBackgroundColor(context.resources.getColor(R.color.cardbackground))
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(mList:List<MenuItemDaftarJual>){
        this.mList = mList
        notifyDataSetChanged()
    }
}