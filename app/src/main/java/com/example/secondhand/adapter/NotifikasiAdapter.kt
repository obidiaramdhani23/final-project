package com.example.secondhand.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.secondhand.R
import com.example.secondhand.databinding.ItemNotifikasiBinding
import com.example.secondhand.model.GetNotificationResponseItem
import com.example.secondhand.viewmodel.NotificationViewModel
import java.text.SimpleDateFormat

class NotifikasiAdapter(
    context: Context,
    list: List<GetNotificationResponseItem>,
    notifviewmodel: NotificationViewModel,
    token: String
): RecyclerView.Adapter<NotifikasiAdapter.ViewHolder>() {
    private var context: Context
    private var list: List<GetNotificationResponseItem>
    private var notifviewmodel: NotificationViewModel
    private var token: String


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotifikasiAdapter.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemNotifikasiBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: NotifikasiAdapter.ViewHolder, position: Int) {
        val data = list.elementAt(position)
        data.let { holder.bind(data) }
        holder.itemView.setOnClickListener (object : View.OnClickListener{
            @SuppressLint("NotifyDataSetChanged")
            override fun onClick(p0: View?) {
                notifviewmodel.patchNotification(token, data.id)
            }

        })
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolder(private val binding: ItemNotifikasiBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(data: GetNotificationResponseItem){
            binding.apply {
                Glide.with(context)
                    .load("${data.imageUrl}")
                    .placeholder(R.drawable.ic_hide_image)
                    .error(R.drawable.ic_hide_image)
                    .centerCrop()
                    .skipMemoryCache(false)
                    .into(ivNotif)
                tvNotifNamaproduk.setText(data.productName)
                tvNotifHarga.setText("Rp${data.basePrice}")
                val parser =  SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.ms'Z'")
                val formatter = SimpleDateFormat("dd-MM, HH:mm")
                val formattedDate = formatter.format(parser.parse(data.createdAt))
                tvNotifWaktu.setText(formattedDate)
                if (data.read) {
                    ivNotifRead.visibility = View.GONE
                }
                when(data.status){
                    "bid" -> {
                        tvNotifStatus.setText("Penawaran produk")
                    }
                    "accepted" -> {
                        tvNotifStatus.setText("Penawaran produk")
                        tvNotifHarga.setPaintFlags(tvNotifHarga.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
                        tvNotifDitawar.setText("Berhasil ditawar Rp${data.bidPrice}")
                        tvNotifDitawar.visibility = View.VISIBLE
                        tvNotifDihubungi.visibility = View.VISIBLE
                    }
                    "create" -> {
                        tvNotifStatus.setText("Berhasil diterbitkan")
                    }
                    "declined" -> {
                        tvNotifStatus.setText("Penawaran ditolak")
                    }
                    else -> {
                        tvNotifStatus.setText(data.status)
                    }
                }
            }
        }
    }
    init {
        this.context = context
        this.list = list
        this.notifviewmodel = notifviewmodel
        this.token = token
    }
}
