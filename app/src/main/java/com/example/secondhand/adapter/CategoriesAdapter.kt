package com.example.secondhand.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.secondhand.R
import com.example.secondhand.model.GetCategoriesItem

class CategoriesAdapter(private var mList: List<GetCategoriesItem>, private var context: Context): RecyclerView.Adapter<CategoriesAdapter.MyHolder>() {
    var row_index = -1
    private var onItemClickCallback: CategoriesAdapter.OnItemClickCallback?=null

//    private val diffCallback = object : DiffUtil.ItemCallback<GetCategoriesItem>(){
//        override fun areItemsTheSame(
//            oldItem: GetCategoriesItem,
//            newItem: GetCategoriesItem
//        ): Boolean {
//            return  oldItem.id == newItem.id
//        }
//
//        override fun areContentsTheSame(
//            oldItem: GetCategoriesItem,
//            newItem: GetCategoriesItem
//        ): Boolean {
//            return oldItem.hashCode() == newItem.hashCode()
//        }
//    }
//
//    private  val differ = AsyncListDiffer(this, diffCallback)

    inner class MyHolder (itemView: View)
        : RecyclerView.ViewHolder(itemView){
        var cardView = itemView.findViewById<CardView>(R.id.carcCategory)
        val titleCategory = itemView.findViewById<TextView>(R.id.tvCategoryTitle)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val holder = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        return MyHolder(holder)
    }

    override fun onBindViewHolder(holder: MyHolder, @SuppressLint("RecyclerView") position: Int) {
        val dataPosition = position
        val data = mList[position]
        holder.cardView.setOnClickListener(object : View.OnClickListener{
            @SuppressLint("NotifyDataSetChanged")
            override fun onClick(p0: View?) {
                onItemClickCallback?.onItemClicked(data)
                row_index = position
                notifyDataSetChanged()
            }
        })
//        when(position){
//            0 -> {
//                holder.titleCategory.setText("Semua")
//            }
//            1-> {
//                differ.currentList[0]
//                holder.titleCategory.setText(data.name)
//            }
//        }
        if (holder.absoluteAdapterPosition == 25){
            holder.titleCategory.setText("Semua")
        }else if (holder.absoluteAdapterPosition < 25){
            holder.titleCategory.setText(data.name)
        }else{
            Toast.makeText(context, "error", Toast.LENGTH_SHORT).show()
        }
        if (row_index == position){
            holder.cardView.setCardBackgroundColor(context.resources.getColor(R.color.purple_200))
            holder.titleCategory.setTextColor(context.resources.getColor(R.color.white))
        }else{
            holder.cardView.setCardBackgroundColor(context.resources.getColor(R.color.cardbackground))
            holder.titleCategory.setTextColor(context.resources.getColor(R.color.black))
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    fun setItem(listItem:List<GetCategoriesItem>){
        this.mList = listItem
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: GetCategoriesItem)
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback){
        this.onItemClickCallback = onItemClickCallback
    }
}