package com.example.secondhand.adapter

import android.content.ClipData
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.secondhand.R
import com.example.secondhand.model.GetProductResponseItem

class SearchAdapter: RecyclerView.Adapter<SearchAdapter.MyViewHolder>() {
    private var listItem : List<GetProductResponseItem> = ArrayList()
    private var onItemClickCallback: SearchAdapter.OnItemClickCallback?=null

    fun setFilteredList(filteredList: List<GetProductResponseItem>) {
        this.listItem = filteredList
        notifyDataSetChanged()
    }
    private val diffCallback = object : DiffUtil.ItemCallback<GetProductResponseItem>(){
        override fun areItemsTheSame(
            oldItem: GetProductResponseItem,
            newItem: GetProductResponseItem
        ): Boolean {
            return  oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: GetProductResponseItem,
            newItem: GetProductResponseItem
        ): Boolean {
            return oldItem.id == newItem.id
        }

    }

    private val differ = AsyncListDiffer(this, diffCallback)

    interface OnItemClickCallback {
        fun onItemClicked(data:GetProductResponseItem)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bind(data: GetProductResponseItem){
            val title = itemView.findViewById<TextView>(R.id.searchItemTitle)
            title.setText(data.name)
            itemView.setOnClickListener { onItemClickCallback?.onItemClicked(data) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val holder = LayoutInflater.from(parent.context).inflate(R.layout.search_item, parent, false)
        return MyViewHolder(holder)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun setItem(listItem:List<GetProductResponseItem>){
        return differ.submitList(listItem)
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback){
        this.onItemClickCallback = onItemClickCallback
    }
}