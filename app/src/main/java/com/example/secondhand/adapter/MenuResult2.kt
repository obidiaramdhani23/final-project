package com.example.secondhand.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.secondhand.R
import com.example.secondhand.databinding.ItemNotifikasiBinding
import com.example.secondhand.fragments.DaftarJualFragmentDirections
import com.example.secondhand.model.GetSellerOrderItem
import java.text.SimpleDateFormat

class MenuResult2(private var mList: List<GetSellerOrderItem>, private val context: Context): RecyclerView.Adapter<MenuResult2.MyHolder>() {
   private var onItemClickCallback : MenuResult2.OnClickCallback?=null

    interface OnClickCallback {
        fun onItemClicked(data:GetSellerOrderItem)
    }
    fun setOnItemClickCallback(onClickCallback: OnClickCallback){
        this.onItemClickCallback = onClickCallback
    }

    inner class MyHolder(private val binding: ItemNotifikasiBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: GetSellerOrderItem){
            Glide.with(context)
                .load(data.product.imageUrl)
                .placeholder(R.drawable.ic_hide_image)
                .error(R.drawable.ic_hide_image)
                .centerCrop()
                .into(binding.ivNotif)
            binding.tvNotifNamaproduk.setText(data.productName)
            binding.tvNotifHarga.setText("Rp " + data.basePrice).toString()
            val parser =  SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.ms'Z'")
            val formatter = SimpleDateFormat("dd-MM, HH:mm")
            val formattedDate = formatter.format(parser.parse(data.createdAt))
            binding.tvNotifWaktu.setText(formattedDate)
            binding.tvNotifStatus.setText("Penawaran Produk")
            binding.tvNotifDitawar.setText("Ditawar Rp ${data.price}")
            binding.tvNotifDitawar.visibility = View.VISIBLE
            binding.ivNotifRead.visibility = View.GONE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MyHolder(ItemNotifikasiBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        val data = mList[position]
        holder.bind(data)
        holder.itemView.setOnClickListener{
            val action = DaftarJualFragmentDirections.actionDaftarJualFragment2ToInfoPenawarFragment(data)
            it.findNavController().navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }
}