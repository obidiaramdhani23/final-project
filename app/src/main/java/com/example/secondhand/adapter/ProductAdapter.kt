package com.example.secondhand.adapter

import android.content.ContentValues
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.example.secondhand.R
import com.example.secondhand.model.GetCategoriesItem
import com.example.secondhand.model.GetProductResponseItem

class ProductAdapter: RecyclerView.Adapter<ProductAdapter.MyViewHolder>() {
    private var onItemClickCallback: ProductAdapter.OnItemClickCallback?=null
    private var listItem: List<GetProductResponseItem> = ArrayList()

    private val diffCallback = object : DiffUtil.ItemCallback<GetProductResponseItem>(){
        override fun areItemsTheSame(
            oldItem: GetProductResponseItem,
            newItem: GetProductResponseItem
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: GetProductResponseItem,
            newItem: GetProductResponseItem
        ): Boolean {
            return oldItem.id == newItem.id
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    interface OnItemClickCallback {
        fun onItemClicked(data: GetProductResponseItem)
    }

    inner class MyViewHolder (itemView: View)
        : RecyclerView.ViewHolder(itemView){
        fun bind(data: GetProductResponseItem){
            val imageProduct = itemView.findViewById<ImageView>(R.id.ivPosterProduct)
            Glide.with(itemView.context)
                .load(data.imageUrl)
                .transform(CenterCrop())
                .into(imageProduct)
            val titleProduct = itemView.findViewById<TextView>(R.id.tvTitleProduct)
            titleProduct.text = data.name
            val titleCategory = itemView.findViewById<TextView>(R.id.tvCategoryTitle)
            titleCategory.setText(data.categories?.elementAtOrNull(0)?.name)
            val priceProduct = itemView.findViewById<TextView>(R.id.tvPrice)
            priceProduct.text = data.basePrice.toString()
            itemView.setOnClickListener{ onItemClickCallback?.onItemClicked(data)}
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val holder = LayoutInflater.from(parent.context).inflate(R.layout.category_item, parent, false)
        return MyViewHolder(holder)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun setItem(listItem:List<GetProductResponseItem>){
        return differ.submitList(listItem)
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback){
        this.onItemClickCallback = onItemClickCallback
    }
}