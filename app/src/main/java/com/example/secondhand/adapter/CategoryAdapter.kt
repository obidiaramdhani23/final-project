package com.example.secondhand.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.secondhand.R
import com.example.secondhand.model.GetCategoryResponseItem
import kotlinx.android.synthetic.main.category_list.view.*


class CategoryAdapter(c: Context, category: List<GetCategoryResponseItem>) :
    RecyclerView.Adapter<CategoryAdapter.Holder>() {
    private var c: Context
    private var category: List<GetCategoryResponseItem>
    private var selectedItemCounter = 0
    var id = arrayListOf(0)

    class Holder: RecyclerView.ViewHolder {
        var checkbox: CheckBox
        constructor(itemView: View):super(itemView){
            checkbox = itemView.findViewById(R.id.category_checkbox)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryAdapter.Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.category_list, parent, false))
    }

    override fun onBindViewHolder(holder: CategoryAdapter.Holder, position: Int) {
        holder.checkbox.category_checkbox.setText(category.elementAt(position).name)
        holder.checkbox.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { _, isChecked->
            if (isChecked) {
                if (selectedItemCounter >= 5) {
                    Toast.makeText(c, "Can't be more than " + selectedItemCounter, Toast.LENGTH_SHORT).show()
                    holder.checkbox.setChecked(false)
                }
                else {
                    selectedItemCounter++
                    id.add(category.elementAt(position).id)
                }
            }
            else if (!isChecked) {
                selectedItemCounter--
                id.remove(category.elementAt(position).id)
            }
        })
    }

    override fun getItemCount(): Int {
        return 18
    }

    init {
        this.c = c
        this.category = category
    }
}