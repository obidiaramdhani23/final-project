package com.example.secondhand.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.secondhand.R
import com.example.secondhand.adapter.ProductAdapter
import com.example.secondhand.databinding.FragmentSearchResultBinding
import com.example.secondhand.model.GetProductResponseItem
import com.example.secondhand.viewmodel.HomeViewModel
import org.koin.android.ext.android.bind
import org.koin.android.ext.android.inject

class SearchResultFragment : Fragment() {

    private var _binding : FragmentSearchResultBinding?=null
    private val binding get() = _binding!!
    private val viewModel : HomeViewModel by inject()
    private val args by navArgs<SearchResultFragmentArgs>()
    private lateinit var productAdapter : ProductAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding =FragmentSearchResultBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        productAdapter = ProductAdapter()
        productAdapter.notifyDataSetChanged()
        if (arguments!=null){
            val keyword = args.currentSearch.name
            viewModel.fetchProductBySearch(keyword, context).observe(viewLifecycleOwner){items->
                if (items != null){
                    setupRecyclerView(items)
                }
            }
        }
        binding.searchView.setOnClickListener {
            findNavController().navigateUp()
        }

    }

    private fun setupRecyclerView(items: List<GetProductResponseItem>) {
        productAdapter.setItem(items)
        productAdapter.setOnItemClickCallback(object : ProductAdapter.OnItemClickCallback{
            override fun onItemClicked(data: GetProductResponseItem) {
                selectData(data)
            }
        })
        binding.rvProduct2.adapter = productAdapter
    }

    private fun selectData(data: GetProductResponseItem) {
        val bundle = Bundle()
        bundle.putParcelable("selectProduct", data)
        val action = SearchResultFragmentDirections.actionSearchResultFragmentToDetailFragment3(data,null)
        findNavController().navigate(action)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SearchResultFragment.
         */
        // TODO: Rename and change types and number of parameters
    }
}