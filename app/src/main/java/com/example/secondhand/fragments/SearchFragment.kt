package com.example.secondhand.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.secondhand.adapter.SearchAdapter
import com.example.secondhand.databinding.FragmentSearchBinding
import com.example.secondhand.model.GetProductResponseItem
import com.example.secondhand.viewmodel.HomeViewModel
import org.koin.android.ext.android.inject
import kotlin.reflect.KMutableProperty0


class SearchFragment : Fragment() {

    private var _binding : FragmentSearchBinding?=null
    private val binding get() = _binding!!
    private lateinit var searchAdapter: SearchAdapter
    private val viewModel : HomeViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchAdapter = SearchAdapter()
        searchAdapter.notifyDataSetChanged()
        fetchData()
    }

    private fun fetchData() {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                binding.searchView.clearFocus()

                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                getData(p0)
                return true
            }

        })
    }

    private fun getData(p0: String?) {
        viewModel.fetchProductBySearch(p0,context).observe(viewLifecycleOwner){ items ->
            if (items!=null){
                setupRecyclerView(items)
            }
        }
    }

    private fun setupRecyclerView(items: List<GetProductResponseItem>) {
        searchAdapter.setItem(items)
        searchAdapter.setOnItemClickCallback(object : SearchAdapter.OnItemClickCallback{
            override fun onItemClicked(data: GetProductResponseItem) {
                selectItem(data)
            }
        })
        binding.rvSearch.adapter = searchAdapter
    }

    private fun selectItem(data: GetProductResponseItem) {
        val bundle = Bundle()
        bundle.putParcelable("searchProduct", data)
        val action = SearchFragmentDirections.actionSearchFragmentToSearchResultFragment(data)
        findNavController().navigate(action)
    }
}