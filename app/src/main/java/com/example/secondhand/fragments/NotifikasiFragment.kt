package com.example.secondhand.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.secondhand.R
import com.example.secondhand.adapter.NotifikasiAdapter
import com.example.secondhand.databinding.FragmentJualBinding
import com.example.secondhand.databinding.FragmentNotifikasiBinding
import com.example.secondhand.model.GetNotificationResponseItem
import com.example.secondhand.viewmodel.DataStoreViewModel
import com.example.secondhand.viewmodel.NotificationViewModel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


class NotifikasiFragment : Fragment() {
    private var _binding: FragmentNotifikasiBinding? = null
    private val binding get() = _binding!!
    private val notifikasiviewmodel: NotificationViewModel by inject()
    private val datastoreviewmodel: DataStoreViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentNotifikasiBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
    }

    private fun observer() {
        datastoreviewmodel.getAccessToken().observe(requireActivity()){
            var token = it
            if (token==""){
                binding.shimmerNotif.apply {
                    visibility = View.GONE
                }
            }
            notifikasiviewmodel.getNotification(token)
            if (isAdded){
                binding.shimmerNotif.startShimmer()
                notifikasiviewmodel.getNotifData.observe(requireActivity()) {
                    if (isAdded) {
                        when (notifikasiviewmodel.getNotifCode) {
                            200 -> {
                                val adapter = NotifikasiAdapter(
                                    requireContext(),
                                    it.sortedByDescending { it.createdAt },
                                    notifikasiviewmodel,
                                    token
                                )
                                binding.rvNotif.setLayoutManager(LinearLayoutManager(requireContext()))
                                binding.rvNotif.adapter = adapter
                                binding.shimmerNotif.apply {
                                    visibility = View.GONE
                                    stopShimmer()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}