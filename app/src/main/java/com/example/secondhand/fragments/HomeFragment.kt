package com.example.secondhand.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.load.engine.Resource
import com.example.secondhand.R
import com.example.secondhand.adapter.*
import com.example.secondhand.adapter.CategoriesAdapter
import com.example.secondhand.adapter.ProductAdapter
import com.example.secondhand.databinding.FragmentHomeBinding
import com.example.secondhand.model.*
import com.example.secondhand.viewmodel.HomeViewModel
import com.smarteist.autoimageslider.SliderView
import org.koin.android.ext.android.inject
import kotlin.collections.ArrayList

class HomeFragment : Fragment() {

    private var _binding : FragmentHomeBinding?=null
    private val binding get() = _binding!!
    private val viewModel : HomeViewModel by inject()
    private lateinit var productAdapter: ProductAdapter
    lateinit var sliderView: SliderView
    lateinit var sliderAdapter: SliderAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        getProduct()
        setBanner()
        binding.cardView2.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragment2ToSearchFragment()
            findNavController().navigate(action)
        }
    }

    private fun setBanner() {
        viewModel.fetchBanner(context).observe(viewLifecycleOwner){items->
            if(items != null){
                sliderAdapter = SliderAdapter( items )
                sliderView = binding.slider
                sliderView.autoCycleDirection = SliderView.LAYOUT_DIRECTION_LTR
                sliderView.setSliderAdapter(sliderAdapter)
                sliderView.scrollTimeInSec = 3
                sliderView.isAutoCycle = true
                sliderView.startAutoCycle()
            }
        }
    }
    @SuppressLint("NotifyDataSetChanged")
    private fun setupAdapter() {
        productAdapter = ProductAdapter()
        productAdapter.notifyDataSetChanged()
        binding.rvProduct.adapter = productAdapter
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getProduct() {
        var per_page = 30
        viewModel.fetchAllProduct(1,30, "available", context)
            .observe(viewLifecycleOwner){items ->
                binding.shimmerLayout.startShimmer()
                binding.shimmerSlider.startShimmer()
                if (items != null) {
                    setupProductRecyclerView(items)
                    binding.shimmerLayout.apply {
                        stopShimmer()
                        visibility = View.GONE
                    }
                    binding.shimmerSlider.apply {
                        stopShimmer()
                        visibility = View.GONE
                    }
                    binding.slider.visibility = View.VISIBLE
                    binding.btnLoad.visibility = View.VISIBLE
                    binding.shimmerSlider.visibility = View.GONE
                    binding.rvProduct.visibility = View.VISIBLE
                }
            }

        binding.btnLoad.setOnClickListener{
            per_page += 30
            viewModel.fetchAllProduct(1, per_page, "available", context)
                .observe(viewLifecycleOwner){ items ->
                    if (items != null){
                        binding.shimmerSlider.visibility = View.GONE
                        setupProductRecyclerView(items)
                    }
                }
        }

        viewModel.fetchAllCategory(context)
            .observe(viewLifecycleOwner){items2 ->
                val array = ArrayList<GetCategoriesItem>()
                array.add(GetCategoriesItem("2022-07-03T14:50:26.765Z", 25, "Semua", "2022-07-03T14:50:26.765Z"))
                array.addAll(items2 as ArrayList<GetCategoriesItem>)
                    val categoriesAdapter = CategoriesAdapter(array, requireContext())
                    categoriesAdapter.setItem(array.sortedByDescending { array.elementAt(0).createdAt })
                    categoriesAdapter.setOnItemClickCallback(object : CategoriesAdapter.OnItemClickCallback{
                        override fun onItemClicked(data: GetCategoriesItem) {
                            selectCategories(data)
                        }
                    })
                    binding.rvCategory.adapter = categoriesAdapter
                    categoriesAdapter.notifyDataSetChanged()

            }
    }

    private fun selectCategories(data: GetCategoriesItem) {
        if(data.id == 25){
            viewModel.fetchAllProduct(1,30, "available", context)
                .observe(viewLifecycleOwner){items ->
                    if (items != null) {
                        binding.shimmerSlider.visibility = View.GONE
                        setupProductRecyclerView(items)
                    }
                }
            var per_page = 30
            binding.btnLoad.setOnClickListener{
                per_page += 30
                viewModel.fetchAllProduct(1, per_page, "available", context)
                    .observe(viewLifecycleOwner){ items ->
                        if (items != null){
                            binding.shimmerSlider.visibility = View.GONE
                            setupProductRecyclerView(items)
                        }
                    }
            }
        }else {
            viewModel.fetchProductByCategoriesId(data.id,1,30, requireContext())
                .observe(viewLifecycleOwner) { items ->
                    if (items != null) {
                        setupProductRecyclerView(items)
                    }
                }
            var per_page = 30
            binding.btnLoad.setOnClickListener{
                per_page += 30
                viewModel.fetchProductByCategoriesId(data.id, 1, per_page ,context)
                    .observe(viewLifecycleOwner){ items ->
                        if (items != null){
                            setupProductRecyclerView(items)
                        }
                    }
            }
        }
    }

    private fun setupProductRecyclerView(items: List<GetProductResponseItem>) {
        productAdapter.setItem(items.take(30))
        productAdapter.setOnItemClickCallback(object : ProductAdapter.OnItemClickCallback{
            override fun onItemClicked(data: GetProductResponseItem) {
                selectProduct(data)
            }
        })
    }

    private fun selectProduct(data: GetProductResponseItem) {
        val bundle = Bundle()
        bundle.putParcelable("selectProduct", data)
        val action = HomeFragmentDirections.actionHomeFragment2ToDetailFragment3(data, null)
        findNavController().navigate(action)
    }
}