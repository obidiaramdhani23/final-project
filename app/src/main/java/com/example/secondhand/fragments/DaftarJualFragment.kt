package com.example.secondhand.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.secondhand.R
import com.example.secondhand.adapter.MenuDaftarJualAdapter
import com.example.secondhand.adapter.MenuResult1
import com.example.secondhand.adapter.MenuResult2
import com.example.secondhand.adapter.NotifikasiAdapter
import com.example.secondhand.daftarjualviewmodel
import com.example.secondhand.databinding.FragmentDaftarJualBinding
import com.example.secondhand.model.*
import com.example.secondhand.viewmodel.DaftarJualViewModel
import com.example.secondhand.viewmodel.DataStoreViewModel
import com.example.secondhand.viewmodel.NotificationViewModel
import com.example.secondhand.viewmodel.SellerProductViewModel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import org.koin.android.ext.android.bind
import org.koin.android.ext.android.inject

class DaftarJualFragment : Fragment() {

    private var _binding: FragmentDaftarJualBinding?=null
    private val binding get() = _binding!!
    private val sellerProductViewModel: SellerProductViewModel by inject()
    private val daftarJualViewModel: DaftarJualViewModel by inject()
    private val datastoreviewmodel: DataStoreViewModel by inject()
    private var token = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDaftarJualBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val notif = activity?.findViewById<CardView>(R.id.notifNavigation)
        val notifX = activity?.findViewById<ImageView>(R.id.notifNavigationX)

        notifX!!.setOnClickListener {
            notif!!.visibility = View.GONE
        }

        datastoreviewmodel.getAccessToken().observe(requireActivity()){
            token = it
            if (it!=""){
                setupMenuResult1()
                setupProfileInfo()
                setupMenu(view)
            }
        }
    }

    private fun setupProfileInfo() {
         datastoreviewmodel.getUserData().observe(viewLifecycleOwner){
             binding.tvPenjual.setText(it.fullName)
             binding.tvKota.setText(it.address)
             if (it.imageUrl!=null){
                 Glide.with(requireContext())
                     .load("${it.imageUrl}")
                     .placeholder(R.drawable.foto_akun)
                     .centerCrop()
                     .into(binding.avaJual)
             }
         }
        binding.btnEdit.setOnClickListener{
            lifecycleScope.launchWhenResumed {
                findNavController().navigate(R.id.editProfileFragment)
            }
        }
    }

    private fun setupMenuResult2(status:String) {
            daftarJualViewModel.getSellerOrder(token, "")
            if (isAdded){
                daftarJualViewModel.getSellerOrderData.observe(requireActivity()) {
                    if (isAdded) {
                        when (daftarJualViewModel.getSellerOrderCode) {
                            200 -> {
                                val dataPending = ArrayList<GetSellerOrderItem>()
                                val dataAccepted = ArrayList<GetSellerOrderItem>()
                                if (!it.isNullOrEmpty()){
                                    for (i in 0 until it.size) {
                                        when (it.elementAt(i).status) {
                                            "pending" -> dataPending.add(it.elementAt(i))
                                            "accepted" -> dataAccepted.add(it.elementAt(i))
                                        }
                                    }
                                }
                                when (status){
                                    "pending" -> {
                                        binding.notifEmpty.visibility = View.GONE
                                        if (dataPending.isNullOrEmpty()){
                                            binding.notifEmptyText.setText("Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok")
                                            binding.notifEmpty.visibility = View.VISIBLE
                                        }
                                        setUpAdapter(dataPending)
                                        dataPending.clear()
                                    }
                                    "accepted" -> {
                                        binding.notifEmpty.visibility = View.GONE
                                        if (dataAccepted.isNullOrEmpty()){
                                            binding.notifEmptyText.setText("Belum ada produkmu yang terjual nih, sabar ya rejeki nggak kemana kok")
                                            binding.notifEmpty.visibility = View.VISIBLE
                                        }
                                        setUpAdapter(dataAccepted)
                                        dataAccepted.clear()
                                    }
                                }
                            }
                        }
                    }
                }
            }
    }

    private fun setupMenuResult1() {
        binding.notifEmpty.visibility = View.GONE
        sellerProductViewModel.getProduct(token)
        if(isAdded||token!=""){
            sellerProductViewModel.getProductResponse.observe(requireActivity()){
                val data = ArrayList<GetSellerProductResponseItem>()
                data.add(GetSellerProductResponseItem(0, listOf(),"","",0,"","","","","","",0))
                if (!it.isNullOrEmpty()){
                    data.addAll(it as ArrayList<GetSellerProductResponseItem>)
                }
                if (isAdded){
                    when(sellerProductViewModel.getProductCode){
                        200 -> {
                            val adapter = MenuResult1()
                            binding.rvMenuResult1.adapter = adapter
                            adapter.setData(data)
                            adapter.setOnItemClickCallback(object : MenuResult1.OnItemClickCallback{
                                override fun onItemClicked(data: GetSellerProductResponseItem) {
                                    if (data.id == 0){
                                        val action = DaftarJualFragmentDirections.actionDaftarJualFragment2ToJualFragment2(null)
                                        findNavController().navigate(action)
                                    }else{
                                        val action = DaftarJualFragmentDirections.actionDaftarJualFragment2ToDetailFragment3(null, data)
                                        findNavController().navigate(action)
                                    }
                                }
                            })
                        }
                    }
                }
            }
        }
    }

    private fun setupMenu(view: View) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.rvMenu)
        val data = ArrayList<MenuItemDaftarJual>()
        val adapter = MenuDaftarJualAdapter(data,requireContext())

        data.add(MenuItemDaftarJual(1,"Produk"))
        data.add(MenuItemDaftarJual(2,"Diminati"))
        data.add(MenuItemDaftarJual(3,"Terjual"))
        recyclerView.adapter = adapter
        adapter.setData(data)
        adapter.setOnItemClickCallback(object : MenuDaftarJualAdapter.OnItemClickCallBack{
            override fun onItemClicked(data: MenuItemDaftarJual) {
                if (data.id == 1){
                    binding.rvMenuResult1.visibility = View.VISIBLE
                    binding.rvMenuResult2.visibility = View.GONE
                    setupMenuResult1()
                } else if ( data.id == 2){
                    binding.rvMenuResult2.visibility = View.VISIBLE
                    binding.rvMenuResult1.visibility = View.GONE
                    setupMenuResult2("pending")
                } else if (data.id == 3){
                    binding.rvMenuResult2.visibility = View.VISIBLE
                    binding.rvMenuResult1.visibility = View.GONE
                    setupMenuResult2("accepted")
                } else{
                    Toast.makeText(requireContext(), "error", Toast.LENGTH_SHORT).show()
                }
            }

        })
    }

    private fun setUpAdapter(it: List<GetSellerOrderItem>){
        val adapter = MenuResult2(
            it.sortedByDescending { it.createdAt },
            requireContext()
        )
        binding.rvMenuResult2.adapter = adapter
    }
}
