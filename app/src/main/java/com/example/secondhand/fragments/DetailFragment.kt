package com.example.secondhand.fragments

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.example.secondhand.LoginActivity
import com.example.secondhand.R
import com.example.secondhand.databinding.FragmentDetailBinding
import com.example.secondhand.dialog.OpenBidDialogFragment
import com.example.secondhand.viewmodel.BuyerOrderViewModel
import com.example.secondhand.viewmodel.DataStoreViewModel
import com.example.secondhand.viewmodel.HomeViewModel
import com.example.secondhand.viewmodel.SellerProductViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import org.koin.android.ext.android.bind
import org.koin.android.ext.android.inject

@Suppress("UNCHECKED_CAST")
class DetailFragment : Fragment() {

    private var _binding : FragmentDetailBinding?=null
    private val binding get() = _binding!!
    private val viewModel : HomeViewModel by inject()
    private val datastoreviewmodel: DataStoreViewModel by inject()
    private val sellerProductViewModel: SellerProductViewModel by inject()
    private val orderViewModel: BuyerOrderViewModel by inject()
    private val args by navArgs<DetailFragmentArgs>()
    private var accesstoken = ""
    private var idProduct : Int ?= null
    private var status = ""
    private lateinit var imageUrl: ArrayList<String?>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupObserver()
        val notif = activity?.findViewById<CardView>(R.id.notifNavigation)
        val notifTv = activity?.findViewById<TextView>(R.id.notifNavigationTv)
        val notifX = activity?.findViewById<ImageView>(R.id.notifNavigationX)
        notifX!!.setOnClickListener {
            notif!!.visibility = View.GONE
        }

//        orderViewModel.getBuyerOrder(accesstoken, args.currentProduct!!.id)

        orderViewModel.getBuyerOrder.observe(viewLifecycleOwner){
            if (it!=null){
                idProduct = it.elementAtOrNull(0)!!.productId
                status = it.elementAtOrNull(0)!!.status
                Toast.makeText(requireContext(), status, Toast.LENGTH_SHORT).show()
                if (status=="pending"){
                    binding.btnNegoBuyer.visibility = View.GONE
                    binding.btnNegoBuyerDone.visibility = View.VISIBLE
                }
            }
        }
        binding.btnNegoBuyer.setOnClickListener {
            if(accesstoken == ""){
                Toast.makeText(requireContext(), "Please Login Before Continue!", Toast.LENGTH_SHORT).show()
                startActivity(Intent(requireContext(), LoginActivity::class.java))
                binding.btnNegoBuyerDone.visibility = View.GONE
            }else {
                val id = args.currentProduct?.id
                if (status == "pending"){
                    Toast.makeText(requireContext(), "wait response", Toast.LENGTH_SHORT).show()
                }else {
                    val args = Bundle()
                    id?.let { it1 -> args.putInt("key", it1) }
                    val newFragment: DialogFragment = OpenBidDialogFragment()
                    newFragment.setArguments(args)
                    newFragment.show(parentFragmentManager, "TAG")
                    binding.btnNegoBuyer.visibility = View.VISIBLE
                    binding.btnNegoBuyerDone.visibility = View.GONE
                }
            }
        }

    }


    private fun setupObserver() {
        datastoreviewmodel.getAccessToken().observe(requireActivity()){
            accesstoken = it
        }
    }

    private fun setupView() {
        if(arguments != null){
            val id = args.currentProduct?.id
            val id2  = args.currentSellerProduct?.id
            if (id != null || id2 == null){
                id?.let {
                    viewModel.fetchProductById(it, context).observe(viewLifecycleOwner){ items ->
                        binding.tvSellerName.setText(items?.elementAtOrNull(0)?.user?.fullName)
                        binding.tvSellerCity.setText(items?.elementAtOrNull(0)?.user?.city)
                        val image = items?.elementAtOrNull(0)?.imageUrl
                        image.let {
                            Glide.with(requireContext())
                                .load(image)
                                .transform(CenterCrop())
                                .into(imageView2)
                        }
                        val avatar = items?.elementAtOrNull(0)?.user?.imageUrl
                        image.let {
                            Glide.with(requireContext())
                                .load(avatar)
                                .transform(CenterCrop())
                                .into(ivSeller)
                        }
                        binding.tvTitleProduct.setText(items?.elementAtOrNull(0)?.name)
                        binding.tvPrice.setText(items?.elementAtOrNull(0)?.basePrice.toString())
                        binding.category.setText(items?.elementAtOrNull(0)?.categories?.elementAtOrNull(0)?.name)
                        binding.description.setText(items?.elementAtOrNull(0)?.description)
                    }
                }
                binding.btnNegoBuyer.visibility = View.VISIBLE
                binding.btnEdit.visibility = View.GONE
                binding.btnHapus.visibility = View.GONE
            } else if(true) {
                id2.let {
                    sellerProductViewModel.getSellerProductById(accesstoken,id2)
                    if (isAdded)
                        sellerProductViewModel.getProductResponse.observe(viewLifecycleOwner){
                            if (isAdded){
                                when(sellerProductViewModel.getProductCode){
                                    200 -> {
                                        Glide.with(requireContext())
                                            .load(it.elementAtOrNull(0)?.imageUrl)
                                            .centerCrop()
                                            .into(binding.imageView2)
                                        binding.tvTitleProduct.setText(it.elementAtOrNull(0)?.name)
                                        binding.category.setText(it.elementAtOrNull(0)?.categories?.elementAtOrNull(0)?.name)
                                        binding.tvPrice.setText(it.elementAtOrNull(0)?.basePrice.toString())
                                        binding.description.setText(it.elementAtOrNull(0)?.description)
                                        val userId = it.elementAtOrNull(0)?.userId
                                        if (userId != null) {
                                            viewModel.fetchUserData(accesstoken,userId, requireContext()).observe(viewLifecycleOwner){
                                                Glide.with(ivSeller.context)
                                                    .load(it?.elementAtOrNull(0)?.imageUrl)
                                                    .transform(CenterCrop())
                                                    .into(ivSeller)

                                                binding.tvSellerName.setText(it?.elementAtOrNull(0)?.fullName)
                                                binding.tvSellerCity.setText(it?.elementAtOrNull(0)?.city.toString())
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    binding.btnNegoBuyer.visibility = View.GONE
                    binding.btnEdit.visibility = View.VISIBLE
                    binding.btnHapus.visibility = View.VISIBLE
                    binding.btnHapus.setOnClickListener {
                        val dialog = AlertDialog.Builder(requireContext())
                        dialog.setTitle("Hapus Produk?")
                        dialog.setMessage("Jika Produk Terhapus, Kamu harus Membuat Postingan Jual Ulang.")
                        dialog.setIcon(R.drawable.ic_baseline_delete_forever_24)
                        dialog.setCancelable(false)
                        dialog.setPositiveButton("Yes, I understand"){dialogInterface, p1 ->
                            sellerProductViewModel.deleteProduct(accesstoken,id2)
                            Toast.makeText(requireContext(), "Product Deleted", Toast.LENGTH_SHORT).show()
                            findNavController().navigate(DetailFragmentDirections.actionDetailFragment3ToDaftarJualFragment2())
                        }
                        dialog.setNegativeButton("I Think for wait buyer for buy myproduct"){dialogInterface, p1 ->
                            Toast.makeText(requireContext(), "Cancle Delete", Toast.LENGTH_SHORT).show()
                        }
                        dialog.show()
                    }
                }
            }
        }
    }
}