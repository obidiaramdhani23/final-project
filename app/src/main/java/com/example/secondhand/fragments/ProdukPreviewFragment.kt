package com.example.secondhand.fragments

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.example.secondhand.R
import com.example.secondhand.databinding.FragmentProdukPreviewBinding
import com.example.secondhand.viewmodel.AuthViewModel
import com.example.secondhand.viewmodel.DataStoreViewModel
import com.example.secondhand.viewmodel.SellerProductViewModel
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.koin.android.ext.android.inject
import java.io.ByteArrayOutputStream


class ProdukPreviewFragment : Fragment() {
    private var _binding: FragmentProdukPreviewBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<ProdukPreviewFragmentArgs>()
    private val sellerproductviewmodel: SellerProductViewModel by inject()
    private val datastoreviewmodel: DataStoreViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProdukPreviewBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val notif = activity?.findViewById<CardView>(R.id.notifNavigation)
        val notifTv = activity?.findViewById<TextView>(R.id.notifNavigationTv)
        val notifX = activity?.findViewById<ImageView>(R.id.notifNavigationX)
        var lokasi = ""

        binding.tvPreviewNamaproduk.setText(args.previewData.namaProduk)
        binding.tvPreviewKategori.setText(args.previewData.kategoriDetail)
        binding.tvPreviewHarga.setText("Rp${args.previewData.harga}")
        binding.tvPreviewDeskripsi.setText(args.previewData.deskripsi)
        binding.ivPreviewProduk.setImageBitmap(args.previewData.image)

        notifX!!.setOnClickListener {
            notif!!.visibility = View.GONE
        }

        datastoreviewmodel.getUserData().observe(requireActivity()){
            if (isAdded){
                binding.tvPreviewNamapenjual.setText(it.fullName)
                binding.tvPreviewKota.setText(it.address)
                Glide.with(requireActivity())
                    .load("${it.imageUrl}")
                    .placeholder(R.drawable.ic_hide_image)
                    .error(R.drawable.ic_hide_image)
                    .centerCrop()
                    .into(binding.ivPreviewProfile)
                lokasi = it.city.toString()
            }
        }

        binding.btnPreviewTerbitkan.setOnClickListener {
            val _view = it
            val baos = ByteArrayOutputStream()
            args.previewData.image?.compress(Bitmap.CompressFormat.PNG, 100, baos)
            val imagearray: ByteArray = baos.toByteArray()

            val image = RequestBody.create("image/png".toMediaTypeOrNull(), imagearray)
            val namaProdukBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), args.previewData.namaProduk)
            val deskripsiBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), args.previewData.deskripsi)
            val hargaBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), args.previewData.harga)
            val imageBody = MultipartBody.Part.createFormData("image", "", image)
            val kategoriBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), args.previewData.kategoriId)
            val lokasibody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "lokasi")

            binding.progressBarPreview.visibility = View.VISIBLE
//            binding.tvPreviewHarga.setText("${args.previewData.namaProduk} ${args.previewData.harga} ${args.previewData.kategoriId} ${args.previewData.deskripsi} ${args.previewData.image} ${args.previewData.accessToken}")
            sellerproductviewmodel.postProduct(args.previewData.accessToken, namaProdukBody, deskripsiBody, hargaBody, kategoriBody, lokasibody, imageBody)

            sellerproductviewmodel.postProductCode.observe(requireActivity()){
                if (it!=null){
                    when (it){
                        201 -> {
                            binding.progressBarPreview.visibility = View.GONE
                            notifTv?.setText("Produk berhasil diterbitkan.")
                            sellerproductviewmodel.postProductCode.postValue(null)
                            lifecycleScope.launchWhenResumed {
                                findNavController().navigateUp()
                                findNavController().popBackStack()
                                findNavController().navigate(R.id.daftarJualFragment2)
                            }
                            notif?.visibility = View.VISIBLE
                        }
                        400 ->{
                            binding.progressBarPreview.visibility = View.GONE
                            notifTv?.setText("Maksimal produk yang bisa dijual 5.")
                            notif?.visibility = View.VISIBLE
                            sellerproductviewmodel.postProductCode.postValue(null)
                        }
                        503 -> {
                            binding.progressBarPreview.visibility = View.GONE
                            notifTv?.setText("Layanan tidak tersedia.")
                            notif?.visibility = View.VISIBLE
                            sellerproductviewmodel.postProductCode.postValue(null)
                        }
                        else -> {
                            binding.progressBarPreview.visibility = View.GONE
                            notifTv?.setText("Error.")
                            notif?.visibility = View.VISIBLE
                            sellerproductviewmodel.postProductCode.postValue(null)
                        }
                    }
                }
            }
        }
    }
}