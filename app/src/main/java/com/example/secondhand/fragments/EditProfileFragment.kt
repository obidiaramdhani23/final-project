package com.example.secondhand.fragments

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.example.secondhand.R
import com.example.secondhand.databinding.FragmentEditProfileBinding
import com.example.secondhand.viewmodel.AuthViewModel
import com.example.secondhand.viewmodel.DataStoreViewModel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.koin.android.ext.android.inject
import java.io.ByteArrayOutputStream


class EditProfileFragment: Fragment() {
    private var _binding: FragmentEditProfileBinding?=null
    private val binding get() = _binding!!
    private val dataStoreViewModel: DataStoreViewModel by inject()
    private val authViewModel: AuthViewModel by inject()
    private val REQUEST_CODE = 100
    private var imageUri: Uri? = null
    private var token =  ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEditProfileBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val notif = activity?.findViewById<CardView>(R.id.notifNavigation)
        val notifTv = activity?.findViewById<TextView>(R.id.notifNavigationTv)
        val notifX = activity?.findViewById<ImageView>(R.id.notifNavigationX)

        dataStoreViewModel.getAccessToken().observe(requireActivity()){
            token = it
        }

        notifX!!.setOnClickListener {
            notif!!.visibility = View.GONE
        }

        binding.imgProf.setOnClickListener {
            openGalery()
        }

        binding.ivLoginArrow.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.btnlogin.setOnClickListener {
            binding.progressBarEdit.visibility = View.VISIBLE
            val baos = ByteArrayOutputStream()
            binding.imgProf.drawable?.toBitmap()?.compress(Bitmap.CompressFormat.PNG, 100, baos)
            val imagearray: ByteArray = baos.toByteArray()
            val nama = binding.etLoginEmail.text.toString()
            val kota = binding.etKota.text.toString()
            val alamat = binding.etAlamat.text.toString()
            val hp = binding.etHp.text.toString()
            val image = RequestBody.create("image/png".toMediaTypeOrNull(),imagearray)

            val namaUserBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(),nama)
            val kotaUserBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(),kota)
            val alamatUserBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(),alamat)
            val hpUserBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(),hp)
            val imageBody = MultipartBody.Part.createFormData("image", "",image)
            authViewModel.getEditUser(token,namaUserBody, kotaUserBody,alamatUserBody,hpUserBody,imageBody)
            binding.progressBarEdit.isClickable = false
        }

        dataStoreViewModel.getUserData().observe(viewLifecycleOwner){
            binding.etLoginEmail.setText(it.fullName)
            binding.etKota.setText(it.city.toString())
            binding.etAlamat.setText(it.address)
            binding.etHp.setText(it.phoneNumber)
            if (!it.imageUrl.isNullOrEmpty()||it.imageUrl!="No data"){
                Glide.with(requireContext())
                    .load("${it.imageUrl}")
                    .placeholder(R.drawable.fi_upload_image_product)
                    .centerCrop()
                    .into(binding.imgProf)
            }
        }

        authViewModel.editUserData.observe(viewLifecycleOwner){
            if (authViewModel.editUserCode != null){
                when(authViewModel.editUserCode){
                    200 -> {
                        authViewModel.getLogin(token)
                        notifTv?.setText("Data berhasil diubah")
                        binding.progressBarEdit.visibility = View.GONE
                        notif?.visibility = View.VISIBLE
                        authViewModel.editUserCode = null
                    }
                    400 -> {
                        notifTv?.setText("Data gagal diubah")
                        notif?.visibility = View.VISIBLE
                        authViewModel.editUserCode = null
                        binding.progressBarEdit.visibility = View.GONE
                    }
                    500 ->{
                        notifTv?.setText("Error")
                        notif?.visibility = View.VISIBLE
                        authViewModel.editUserCode = null
                        binding.progressBarEdit.visibility = View.GONE
                    }
                    else -> {
                        notifTv?.setText("${authViewModel.editUserCode} Error put data")
                        notif?.visibility = View.VISIBLE
                        binding.progressBarEdit.visibility = View.GONE
                        authViewModel.editUserCode = null
                    }
                }
            }
        }
    }

    private fun openGalery(){
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        val mimeTypes = arrayOf("image/jpeg", "image/png", "image/jpg")
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
            if (data?.getData() != null){
                imageUri = data.data!!
                binding.imgProf.setImageURI(imageUri)
            }
        }
    }
}