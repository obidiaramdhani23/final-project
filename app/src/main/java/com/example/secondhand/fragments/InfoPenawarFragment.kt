package com.example.secondhand.fragments

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavArgs
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.secondhand.R
import com.example.secondhand.databinding.FragmentInfoPenawarBinding
import com.example.secondhand.model.GetProductByIdResponse
import com.example.secondhand.viewmodel.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat


class InfoPenawarFragment : Fragment() {
    private var _binding : FragmentInfoPenawarBinding? = null
    private val binding get() = _binding!!
    private val dataStoreViewModel: DataStoreViewModel by inject()
    private val sellerorderviewmodel: SellerOrderViewModel by inject()
    private val sellerproductviewmodel: SellerProductViewModel by inject()
    private var accesstoken = ""
    private val args by navArgs<InfoPenawarFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInfoPenawarBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()

        binding.ivInfopenawarArrow.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.tvInfopenawarNamapenjual.setText(args.infoPenawarData.user.fullName)
        binding.tvInfopenawarKota.setText(args.infoPenawarData.user.city)
        binding.tvNotifNamaproduk.setText(args.infoPenawarData.productName)
        binding.tvNotifHarga.setText("Rp${args.infoPenawarData.basePrice}")
        binding.tvNotifDitawar.setText("Berhasil ditawar Rp${args.infoPenawarData.price}")
        val parser =  SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.ms'Z'")
        val formatter = SimpleDateFormat("dd-MM, HH:mm")
        val formattedDate = formatter.format(parser.parse(args.infoPenawarData.createdAt))
        binding.tvNotifWaktu.setText(formattedDate)
        Glide.with(requireContext())
            .load("${args.infoPenawarData.imageProduct}")
            .placeholder(R.drawable.ic_hide_image)
            .error(R.drawable.ic_hide_image)
            .centerCrop()
            .into(binding.ivInfopenawarProduct)

        when (args.infoPenawarData.status){
            "pending" -> {
                binding.btnInfopenawarLeft.setText("Tolak")
                binding.btnInfopenawarRight.setText("Terima")
            }
            else -> {
                binding.btnInfopenawarLeft.setText("Status")
                binding.btnInfopenawarRight.setText("Hubungi")
            }
        }

        when (binding.btnInfopenawarLeft.text){
            "Tolak" -> {
                binding.btnInfopenawarLeft.setText("Tolak")
                binding.btnInfopenawarRight.setText("Terima")

                binding.btnInfopenawarRight.setOnClickListener {
                    sellerorderviewmodel.patchOrder(accesstoken, args.infoPenawarData.id, RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "accepted"))
                    binding.progressBarInfopenawar.visibility = View.VISIBLE
                }
                binding.btnInfopenawarLeft.setOnClickListener {
                    sellerorderviewmodel.patchOrder(accesstoken, args.infoPenawarData.id, RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "declined"))
                    binding.progressBarInfopenawar.visibility = View.VISIBLE
                }
            }
            "Status" -> {
                binding.btnInfopenawarLeft.setText("Status")
                binding.btnInfopenawarRight.setText("Hubungi")

                binding.btnInfopenawarRight.setOnClickListener {
                    val number = args.infoPenawarData.user.phoneNumber
                    number.replace("+","")
                    number.replace(" ","")
                    val url = "https://api.whatsapp.com/send?phone=$number"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
                binding.btnInfopenawarLeft.setOnClickListener {
                    val bottomSheetDialog = BottomSheetDialog(requireActivity())
                    bottomSheetDialog.setContentView(R.layout.fragment_product_status_dialog)
                    val button = bottomSheetDialog.findViewById<Button>(R.id.btn_productstatus)
                    val radioGroup = bottomSheetDialog.findViewById<RadioGroup>(R.id.radiogroup)
                    radioGroup?.setOnCheckedChangeListener(
                        RadioGroup.OnCheckedChangeListener { group, checkedId ->
                            val radio = bottomSheetDialog.findViewById<RadioButton>(checkedId)
                            button?.isClickable = true
                            button?.setBackgroundColor(Color.parseColor("#7126B5"))
                        }
                    )
                    button?.setOnClickListener {
                        if (button.isClickable){
                            var id: Int = radioGroup!!.checkedRadioButtonId
                            if (id!=-1&&isAdded){
                                val radio: RadioButton? = bottomSheetDialog.findViewById(id)
                                if (radio!!.text=="Berhasil terjual"){
                                    binding.progressBarInfopenawar.visibility = View.VISIBLE
                                    sellerproductviewmodel.patchProduct(accesstoken, args.infoPenawarData.productId, RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "sold"))
                                    bottomSheetDialog.dismiss()
                                }
                                else if (radio!!.text=="Batalkan transaksi"){
                                    binding.progressBarInfopenawar.visibility = View.VISIBLE
                                    sellerproductviewmodel.patchProduct(accesstoken, args.infoPenawarData.productId, RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "available"))
                                    bottomSheetDialog.dismiss()
                                }
                            }
                        }
                    }
                    bottomSheetDialog.show()
                }
            }
        }
    }

    private fun observer() {
        dataStoreViewModel.getAccessToken().observe(requireActivity()){
            accesstoken = it
        }

        sellerorderviewmodel.patchOrderCode.observe(requireActivity()){
            if (it!=null&&isAdded){
                when(it){
                    200 -> {
                        sellerorderviewmodel.patchOrderResponse.observe(requireActivity()){
                            when (it.status){
                                "accepted" -> {
                                    val bottomSheetDialog = BottomSheetDialog(requireActivity())
                                    bottomSheetDialog.setContentView(R.layout.fragment_accept_offer_dialog)
                                    val button = bottomSheetDialog.findViewById<Button>(R.id.btn_acceptoffer)
                                    Glide.with(requireContext())
                                        .load("${args.infoPenawarData.imageProduct}")
                                        .placeholder(R.drawable.ic_hide_image)
                                        .error(R.drawable.ic_hide_image)
                                        .centerCrop()
                                        .into(bottomSheetDialog.findViewById(R.id.iv_acceptoffer_product)!!)
                                    bottomSheetDialog.findViewById<ImageView>(R.id.iv_acceptoffer_user)
                                    bottomSheetDialog.findViewById<TextView>(R.id.tv_acceptoffer_nama)?.setText(args.infoPenawarData.user.fullName)
                                    bottomSheetDialog.findViewById<TextView>(R.id.tv_acceptoffer_Kota)?.setText(args.infoPenawarData.user.city)
                                    bottomSheetDialog.findViewById<TextView>(R.id.tv_acceptoffer_namaproduk)?.setText(args.infoPenawarData.productName)
                                    bottomSheetDialog.findViewById<TextView>(R.id.tv_acceptoffer_harga)?.setText("Rp${args.infoPenawarData.basePrice}")
                                    bottomSheetDialog.findViewById<TextView>(R.id.tv_acceptoffer_ditawar)?.setText("Ditawar Rp${args.infoPenawarData.price}")
                                    button?.setOnClickListener {
                                        val number = args.infoPenawarData.user.phoneNumber
                                        val url = "https://api.whatsapp.com/send?phone=$number"
                                        val i = Intent(Intent.ACTION_VIEW)
                                        i.data = Uri.parse(url)
                                        startActivity(i)
                                        bottomSheetDialog.dismiss()
                                    }
                                    binding.progressBarInfopenawar.visibility = View.GONE
                                    bottomSheetDialog.show()
                                    sellerorderviewmodel.patchOrderCode.value = null
                                }
                                "declined" -> {
                                    binding.progressBarInfopenawar.visibility = View.GONE
                                    sellerorderviewmodel.patchOrderCode.value = null
                                    if (isAdded) {
                                        findNavController().navigate(R.id.daftarJualFragment2)
                                    }
                                    Toast.makeText(requireContext(), "Berhasil ditolak", Toast.LENGTH_SHORT).show()
                                }
                            }
                        }
                    }
                    else -> Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show()
                }
            }
        }

        sellerproductviewmodel.patchProductCode.observe(requireActivity()){
            if (it != null) {
                when (it) {
                    200 -> {
                        binding.progressBarInfopenawar.visibility = View.GONE
                        sellerproductviewmodel.patchProductCode.value = null
                        if (isAdded) {
                            findNavController().navigate(R.id.daftarJualFragment2)
                        }
                    }
                    400 -> {
                        Toast.makeText(
                            requireContext(),
                            "Produk tidak ditemukan",
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.progressBarInfopenawar.visibility = View.GONE
                        sellerorderviewmodel.patchOrderCode.value = null
                    }
                    else -> {
                        Toast.makeText(
                            requireContext(),
                            "Error $it",
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.progressBarInfopenawar.visibility = View.GONE
                        sellerorderviewmodel.patchOrderCode.value = null
                    }
                }
            }
        }
    }
}