package com.example.secondhand.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.example.secondhand.BottomNavActivity
import com.example.secondhand.LoginActivity
import com.example.secondhand.R
import com.example.secondhand.SplashActivity
import com.example.secondhand.databinding.FragmentAkunBinding
import com.example.secondhand.model.GetUserData
import com.example.secondhand.viewmodel.AuthViewModel
import com.example.secondhand.viewmodel.DataStoreViewModel
import kotlinx.android.synthetic.main.fragment_akun.view.*
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


class AkunFragment : Fragment() {
    private var _binding: FragmentAkunBinding? = null
    private val binding get() = _binding!!
    private val datastoreviewmodel: DataStoreViewModel by inject()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAkunBinding.inflate(layoutInflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.txtUbah.setOnClickListener{
            val actionToEditProfile = AkunFragmentDirections.actionAkunFragment2ToEditProfileFragment()
            it?.findNavController()?.navigate(actionToEditProfile)
        }
        binding.textSetting.setOnClickListener {

        }
        binding.textLogout.setOnClickListener {
            datastoreviewmodel.saveAccesToken("")
            datastoreviewmodel.saveUserData(GetUserData("No data","No data","No data","No data","No data",-1,"No data","No data","No data","No data"))
            startActivity(Intent(requireContext(), SplashActivity::class.java))
            requireActivity().finishAffinity()
        }

        datastoreviewmodel.getUserData().observe(viewLifecycleOwner){
            if (it!=null){
                if (it.imageUrl != null || it.imageUrl != "No data") {
                    Glide.with(requireContext())
                        .load(it.imageUrl)
                        .placeholder(R.drawable.foto_akun)
                        .transform(CenterCrop())
                        .into(binding.imgProf)
                }
            }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}