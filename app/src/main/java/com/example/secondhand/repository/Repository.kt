package com.example.secondhand.repository

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.secondhand.model.*
import com.example.secondhand.network.ApiService
import com.example.secondhand.room.Product
import com.example.secondhand.room.ProductDao
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository(private val apiService: ApiService) {
    var listProduct = MutableLiveData<List<GetProductResponseItem>?>()
    var listCategory = MutableLiveData<List<GetCategoriesItem>?>()
    var listUser = MutableLiveData<List<GetUserData>?>()
    var listBanner = MutableLiveData<List<GetBannerHomepageItem>?>()
    var listProductById = MutableLiveData<List<GetProductByIdResponse>?>()

//    fun instert(item: GetProductResponseItem){
//        Log.d("State", "Inserting Data")
//        InsertItemAsyncTask(
//            productDao
//        ).execute(item)
//    }
//
//    private fun deleteAllProduct(){
//        DeleteAllItemAsyncTask(
//            productDao
//        ).execute()
//    }
//
//    fun getAllProduct(): LiveData<List<GetProductResponseItem>>{
//        return productDao.getAllProduct()
//    }
//
//    private class InsertItemAsyncTask(val productDao: ProductDao): AsyncTask<GetProductResponseItem, Unit, Unit>(){
//        override fun doInBackground(vararg item: GetProductResponseItem?) {
//            productDao.insertProduct(item[0]!!)
//        }
//    }
//
//    private class DeleteAllItemAsyncTask(val productDao: ProductDao): AsyncTask<Unit,Unit,Unit>(){
//        override fun doInBackground(vararg p0: Unit?) {
//            productDao.deleteAllProduct()
//        }
//    }

    fun fetchAllProduct(page:Int,per_page:Int, status:String, context: Context?): MutableLiveData<List<GetProductResponseItem>?> {
//        deleteAllProduct()
        apiService.getProduct(page, per_page, status)
            .enqueue(object : Callback<GetProductResponse>{
                override fun onResponse(
                    call: Call<GetProductResponse>,
                    response: Response<GetProductResponse>
                ) {
                    if (response.isSuccessful){
                        val responseBody = response.body()
                        if (responseBody!=null){
                            listProduct.postValue(responseBody)
//                            val listData = responseBody
//                            listData?.let {
//                                for (item in it){
//                                    instert(item)
//                                }
//                            }
                        } else {
                            Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                        }
                    }else{
                        Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<GetProductResponse>, t: Throwable) {
                    Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                }

            })
        return listProduct
    }

    fun fetchListCategory(context: Context?): MutableLiveData<List<GetCategoriesItem>?> {
        apiService.getCategory()
            .enqueue(object : Callback<GetCategories>{
                override fun onResponse(
                    call: Call<GetCategories>,
                    response: Response<GetCategories>
                ) {
                    if (response.isSuccessful){
                        val responseBody = response.body()
                        if (responseBody!=null){
                            listCategory.postValue(responseBody)
                        }else{
                            Toast.makeText(context, "error fetch category", Toast.LENGTH_LONG).show()
                        }
                    }else {
                        Toast.makeText(context, "error fetch category", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<GetCategories>, t: Throwable) {
                    Toast.makeText(context, "error fetch category", Toast.LENGTH_LONG).show()
                }

            })
        return listCategory
    }
    fun fetchProductByCategoriesId(CategoryId: Int,page: Int,per_page: Int, context: Context?): MutableLiveData<List<GetProductResponseItem>?> {
        apiService.getProductbyCategoriesId(CategoryId, page, per_page)
            .enqueue(object : Callback<GetProductResponse>{
                override fun onResponse(
                    call: Call<GetProductResponse>,
                    response: Response<GetProductResponse>
                ) {
                    if (response.isSuccessful){
                        val responseBody = response.body()
                        if (responseBody!=null){
                            listProduct.postValue(responseBody)
                        } else {
                            Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                        }
                    }else{
                        Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<GetProductResponse>, t: Throwable) {
                    Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                }

            })
        return listProduct
    }

    fun fetchProductById(id: Int, context: Context?): MutableLiveData<List<GetProductByIdResponse>?>{
        apiService.getProductById(id)
            .enqueue(object : Callback<GetProductByIdResponse>{
                override fun onResponse(
                    call: Call<GetProductByIdResponse>,
                    response: Response<GetProductByIdResponse>
                ) {
                    if(response.isSuccessful){
                        val responseBody = response.body()
                        if (responseBody != null){
                            listProductById.postValue((listOf(responseBody)))
                        }else{
                            Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                        }
                    }else
                        Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                }
                override fun onFailure(call: Call<GetProductByIdResponse>, t: Throwable) {
                    Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                }
            })
        return listProductById
    }

    fun fetchUserData(access_token: String, userId : Int, context: Context?):MutableLiveData<List<GetUserData>?>{
        apiService.getUser(access_token, userId)
            .enqueue(object : Callback<GetUserData>{
                override fun onResponse(call: Call<GetUserData>, response: Response<GetUserData>) {
                    if(response.isSuccessful){
                        val responseBody = response.body()
                        if (responseBody != null) {
                            listUser.postValue(listOf(responseBody))
                        }else{
                            Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                        }
                    }else{
                        Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<GetUserData>, t: Throwable) {
                    Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                }

            })
        return listUser
    }

    fun fetchProductBySearch(search : String?, context: Context?):MutableLiveData<List<GetProductResponseItem>?>{
        apiService.getProductbySearch(search)
            .enqueue(object : Callback<GetProductResponse>{
                override fun onResponse(
                    call: Call<GetProductResponse>,
                    response: Response<GetProductResponse>
                ) {
                    if(response.isSuccessful){
                        val responseBody = response.body()
                        if (responseBody != null){
                            listProduct.postValue(responseBody)
                        }else {
                            Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                        }
                    }else{
                        Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<GetProductResponse>, t: Throwable) {
                    Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                }

            })
        return listProduct
    }

    fun fetchAllBanner(context: Context?):MutableLiveData<List<GetBannerHomepageItem>?>{
        apiService.getBanner()
            .enqueue(object : Callback<GetBannerHomepage>{
                override fun onResponse(
                    call: Call<GetBannerHomepage>,
                    response: Response<GetBannerHomepage>
                ) {
                    if (response.isSuccessful){
                        val responseBody = response.body()
                        if (responseBody != null){
                            listBanner.postValue(responseBody)
                        }else {
                            Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                        }
                    }else{
                        Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<GetBannerHomepage>, t: Throwable) {
                    Toast.makeText(context, "error fetch data", Toast.LENGTH_LONG).show()
                }

            })
        return listBanner
    }
}