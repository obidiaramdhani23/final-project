package com.example.secondhand

import android.R
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.core.view.marginTop
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.example.secondhand.databinding.ActivityRegisterBinding
import com.example.secondhand.model.RegisterRequest
import com.example.secondhand.viewmodel.AuthViewModel
import com.google.android.material.color.MaterialColors
import org.koin.android.ext.android.inject


class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    private val authviewmodel: AuthViewModel by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivRegisterArrow.setOnClickListener {
            finish()
        }

        binding.btnregister.setOnClickListener {
            binding.textInputLayout0.error = null
            binding.textInputLayout1.error = null
            binding.textInputLayout2.error = null
            val nama = binding.etRegisterNama.text
            val email = binding.etRegisterEmail.text
            val password = binding.etRegisterPassword.text
            val request = RegisterRequest(nama.toString(), email.toString(), password.toString())

            if (nama.isNullOrEmpty()||email.isNullOrEmpty()||password.isNullOrEmpty()) {
                binding.textInputLayout0.error = "Masukkan nama"
                binding.textInputLayout1.error = "Masukkan email"
                binding.textInputLayout2.error = "Masukkan password"
            }
            else {
                if (password.length<6) {
                    binding.textInputLayout2.error = "Password minimal 6 karakter"
                }
                else {
                    binding.progressBarRegister.visibility = View.VISIBLE
                    authviewmodel.postregister(request)
                }
            }
        }

        binding.notifRegisterX.setOnClickListener {
            binding.notifRegister.visibility = View.GONE
        }

        authviewmodel.postRegisterData.observe(this, Observer {
            if (authviewmodel.postRegisterCode != null){
                when(authviewmodel.postRegisterCode){
                    201 -> {
                        binding.progressBarRegister.visibility = View.GONE
                        binding.etRegisterNama.setText("")
                        binding.etRegisterEmail.setText("")
                        binding.etRegisterPassword.setText("")
                        binding.notifRegister.visibility = View.VISIBLE
                        authviewmodel.postRegisterCode = null
                    }
                    400 -> {
                        binding.progressBarRegister.visibility = View.GONE
                        binding.textInputLayout1.error = "Email sudah terdaftar"
                        authviewmodel.postRegisterCode = null
                    }
                    500 ->{
                        binding.progressBarRegister.visibility = View.GONE
                        Toast.makeText(this, "Service tidak tersedia", Toast.LENGTH_SHORT).show()
                        authviewmodel.postRegisterCode = null
                    }
                    else -> {
                        binding.tvRegister.setText(authviewmodel.postRegisterCode.toString())
                        authviewmodel.postRegisterCode = null
                    }
                }
            }
        })

        binding.tvRegisterTologin.setOnClickListener {
            finish()
        }
    }
}