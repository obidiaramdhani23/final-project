package com.example.secondhand.room

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.secondhand.model.GetProductResponseItem


class Product (var product: GetProductResponseItem){
    @PrimaryKey(autoGenerate = true)
    var id: Int = product.id
}