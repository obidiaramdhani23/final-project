package com.example.secondhand.room

import androidx.room.TypeConverter
import com.example.secondhand.model.GetProductResponseItem
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class CategoryConverter {

    var gson: Gson = Gson()

    @TypeConverter
    fun stringToCategoryList(data: String): GetProductResponseItem{
        val listType = object : TypeToken<GetProductResponseItem>(){}.type
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun categoryListToString(product: GetProductResponseItem):String?{
        return gson.toJson(product)
    }

}