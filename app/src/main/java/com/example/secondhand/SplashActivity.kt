package com.example.secondhand

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.example.secondhand.viewmodel.DataStoreViewModel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class SplashActivity : AppCompatActivity() {
    private val datastoreviewmodel: DataStoreViewModel by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        var isLoggedIn: String? = null

        datastoreviewmodel.getAccessToken().observe(this){
            isLoggedIn = it
        }

        Handler().postDelayed({
            if (isLoggedIn!=null){
                when (isLoggedIn) {
                    "" -> {
                        BottomNavActivity.open(this, isLoggedIn)
                        isLoggedIn = null
                    }
                    else -> {
                        BottomNavActivity.open(this, isLoggedIn)
                        isLoggedIn = null
                    }
                }
            }
            finish()
        }, 2000)
    }
}