package com.example.secondhand.manager

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.example.secondhand.model.GetUserData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DataStoreManager(context: Context) {
    private val context = context

    suspend fun setAccessToken(status: String) {
        context.userDataStore.edit { preferences ->
            val key = stringPreferencesKey("AccessToken")
            preferences[key] = status
        }
    }

    fun getAccessToken(): Flow<String> {
        return context.userDataStore.data.map { preferences ->
            val key = stringPreferencesKey("AccessToken")
            preferences[key] ?: "" }
    }

    suspend fun setUserData(status: GetUserData) {
        context.userDataStore.edit { preferences ->
            val account_id = intPreferencesKey("account_id")
            val account_name = stringPreferencesKey("account_name")
            val account_email = stringPreferencesKey("account_email")
            val account_password = stringPreferencesKey("account_password")
            val account_address = stringPreferencesKey("account_address")
            val account_city = stringPreferencesKey("account_city")
            val account_created = stringPreferencesKey("account_created")
            val account_updated = stringPreferencesKey("account_updated")
            val account_imageurl = stringPreferencesKey("account_imageurl")
            val account_phone = stringPreferencesKey("account_phone")
            preferences[account_id] = status.id
            preferences[account_name] = status.fullName
            preferences[account_email] = status.email
            preferences[account_password] = status.password
            preferences[account_address] = status.address
            preferences[account_city] = status.city.toString()
            preferences[account_created] = status.createdAt
            preferences[account_updated] = status.updatedAt
            preferences[account_imageurl] = status.imageUrl.toString()
            preferences[account_phone] = status.phoneNumber
        }
    }

    fun getUserData(): Flow<GetUserData> {
        return context.userDataStore.data.map { preferences ->
            val account_id = intPreferencesKey("account_id")
            val account_name = stringPreferencesKey("account_name")
            val account_email = stringPreferencesKey("account_email")
            val account_password = stringPreferencesKey("account_password")
            val account_address = stringPreferencesKey("account_address")
            val account_city = stringPreferencesKey("account_city")
            val account_created = stringPreferencesKey("account_created")
            val account_updated = stringPreferencesKey("account_updated")
            val account_imageurl = stringPreferencesKey("account_imageurl")
            val account_phone = stringPreferencesKey("account_phone")
           GetUserData(
                preferences[account_address]!!,
                preferences[account_city]!!,
                preferences[account_created]!!,
                preferences[account_email]!!,
                preferences[account_name]!!,
                preferences[account_id]!!,
                preferences[account_imageurl] ?: "",
                preferences[account_password]!!,
                preferences[account_phone]!!,
                preferences[account_updated]!!,)
            }
    }

    companion object {
        private const val DATASTORE_NAME = "kel2_secondhand"
        private val Context.userDataStore by preferencesDataStore(
            name = DATASTORE_NAME
        )
    }
}