package com.example.secondhand

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.secondhand.databinding.ActivityLoginBinding
import com.example.secondhand.model.LoginRequest
import com.example.secondhand.model.RegisterRequest
import com.example.secondhand.viewmodel.AuthViewModel
import com.example.secondhand.viewmodel.DataStoreViewModel
import org.koin.android.ext.android.inject

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private val authviewmodel: AuthViewModel by inject()
    private val datastoreviewmodel: DataStoreViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnlogin.setOnClickListener {
            binding.textInputLayout.error = null
            binding.textInputLayout2.error = null

            val email = binding.etLoginEmail.text
            val password = binding.etLoginPassword.text
            val request = LoginRequest(email.toString(), password.toString())

            if (email.isNullOrEmpty()||password.isNullOrEmpty()){
                binding.textInputLayout.error = "Masukkan email"
                binding.textInputLayout2.error = "Masukkan password"
            }
            else {
                if (password.length<6)
                    binding.textInputLayout2.error = "Password minimal 6 karakter"
                else {
                    binding.progressBarLogin.visibility = View.VISIBLE
                    binding.loginview.isClickable = false
                    authviewmodel.postlogin(request)
                }
            }
        }

        authviewmodel.postLoginData.observe(this, Observer {
            if (authviewmodel.postLoginCode != null){
                when(authviewmodel.postLoginCode){
                    201 -> {
                        binding.progressBarLogin.visibility = View.GONE
                        authviewmodel.getLogin(authviewmodel.postLoginData.value!!.access_token)
                        datastoreviewmodel.saveAccesToken(authviewmodel.postLoginData.value!!.access_token)
                        startActivity(Intent(this, SplashActivity::class.java))
                        authviewmodel.postLoginCode = null
                        finishAffinity()
                    }
                    401 -> {
                        binding.progressBarLogin.visibility = View.GONE
                        binding.textInputLayout.error = "Email/Password salah"
                        binding.textInputLayout2.error = "Email/Password salah"
                        authviewmodel.postLoginCode = null
                    }
                    500 ->{
                        binding.progressBarLogin.visibility = View.GONE
                        Toast.makeText(this, "Service tidak tersedia", Toast.LENGTH_SHORT).show()
                        authviewmodel.postLoginCode = null
                    }
                }
            }
        })

        binding.tvLoginToregister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
        binding.ivLoginArrow.setOnClickListener {
            finish()
        }
    }
}