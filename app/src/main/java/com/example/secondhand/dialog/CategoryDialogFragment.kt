package com.example.secondhand.dialog

import android.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.secondhand.adapter.CategoryAdapter
import com.example.secondhand.databinding.FragmentCategoryDialogBinding

import com.example.secondhand.model.GetCategoryResponseItem
import com.example.secondhand.viewmodel.CategoryViewModel
import kotlinx.android.synthetic.main.fragment_category_dialog.view.*
import kotlinx.android.synthetic.main.fragment_jual.view.*
import org.koin.android.ext.android.inject


class CategoryDialogFragment(category: List<GetCategoryResponseItem>) : DialogFragment() {
    private val categoryviewmodel: CategoryViewModel by inject()

    var category = category
    var rv: RecyclerView? = null
    var adapter: CategoryAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView: View = inflater.inflate(com.example.secondhand.R.layout.fragment_category_dialog, container)

        //RECYCLER
        rv = rootView.findViewById<View>(com.example.secondhand.R.id.category_recyclerview) as RecyclerView
        rv!!.layoutManager = LinearLayoutManager(this.activity)

        //ADAPTER
        adapter = CategoryAdapter(requireContext(), category)
        rootView.tv_kategori_ok.setOnClickListener {
            this.dialog?.dismiss()
            adapter!!.id.removeAt(0)
            categoryviewmodel.ids.value = adapter!!.id
        }
        rv!!.adapter = adapter
        this.dialog!!.setTitle("TV Shows")
        return rootView
    }
}