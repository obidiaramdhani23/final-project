package com.example.secondhand.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegisterRequest (
    val full_name: String,
    val email: String,
    val password: String,
    val phone_number: String = "0",
    val address: String = "-",
    val image: String = "",
    val city: String = "-"
) : Parcelable