package com.example.secondhand.model


import com.google.gson.annotations.SerializedName

class GetNotificationResponse : ArrayList<GetNotificationResponseItem>()