package com.example.secondhand.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EditUserRequest (
    val full_name: String? = null,
    val city : String? = null,
    val address : String? = null,
    val phone_number : String? = null,

        ):Parcelable