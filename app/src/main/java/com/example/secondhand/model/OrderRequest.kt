package com.example.secondhand.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderRequest(
    val product_id : Int,
    val bid_price: Int
): Parcelable