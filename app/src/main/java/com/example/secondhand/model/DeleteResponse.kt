package com.example.secondhand.model

import com.google.gson.annotations.SerializedName

data class DeleteResponse(
    @SerializedName("name")
    val name: String,
    @SerializedName("message")
    val message: String
    )