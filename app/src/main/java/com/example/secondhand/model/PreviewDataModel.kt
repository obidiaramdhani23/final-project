package com.example.secondhand.model

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class PreviewDataModel (
    val accessToken: String,
    val namaProduk: String,
    val kategoriId: String,
    val kategoriDetail: String,
    val harga: String,
    val deskripsi: String,
    val image: Bitmap?
    ):Parcelable