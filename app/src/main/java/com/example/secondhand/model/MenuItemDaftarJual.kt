package com.example.secondhand.model

data class MenuItemDaftarJual(
    val id: Int,
    val title: String
)