package com.example.secondhand.model


import com.google.gson.annotations.SerializedName

data class GetCategoriesItem(
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("updatedAt")
    val updatedAt: String
)