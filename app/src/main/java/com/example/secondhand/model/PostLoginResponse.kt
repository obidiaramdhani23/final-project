package com.example.secondhand.model

import com.google.gson.annotations.SerializedName

data class PostLoginResponse(
    @SerializedName("email")
    val email: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("access_token")
    val access_token: String
)
