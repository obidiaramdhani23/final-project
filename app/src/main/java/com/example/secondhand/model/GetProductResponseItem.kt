package com.example.secondhand.model


import android.os.Parcelable
import androidx.room.*
import com.example.secondhand.room.CategoryConverter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "tb_product")
@Parcelize
data class GetProductResponseItem(
    @SerializedName("base_price")
    var basePrice: Int,
    @SerializedName("Categories")
    var categories: List<Category>?,
    @SerializedName("createdAt")
    val createdAt: String,
    @SerializedName("description")
    val description: String?,
    @SerializedName("id")
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @SerializedName("image_name")
    val imageName: String,
    @SerializedName("image_url")
    var imageUrl: String,
    @SerializedName("location")
    val location: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("updatedAt")
    var updatedAt: String,
    @SerializedName("user_id")
    val userId: Int,
) : Parcelable