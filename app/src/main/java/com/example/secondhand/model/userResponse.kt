package com.example.secondhand.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserResponse (
        val id: Int? = null,
        val full_name: String? = null,
        val email: String? = null,
        val password : String? = null,
        val phone_number : String? = null,
        val city : String? = null,
        val address : String? = null,
        val image_url : String? = null,
        val createdAt : String? = null,
        val updatedAt : String? = null
):Parcelable