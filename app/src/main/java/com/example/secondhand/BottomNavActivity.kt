package com.example.secondhand

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.secondhand.databinding.ActivityBottomNavBinding
import com.example.secondhand.model.GetUserData
import com.example.secondhand.viewmodel.AuthViewModel
import com.example.secondhand.viewmodel.CategoryViewModel
import com.example.secondhand.viewmodel.DataStoreViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.koin.android.ext.android.inject

class BottomNavActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBottomNavBinding
    private val datastoreviewmodel: DataStoreViewModel by inject()
    private val categoryviewmodel: CategoryViewModel by inject()
    private val authviewmodel: AuthViewModel by inject()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBottomNavBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val bottomNavView = findViewById<BottomNavigationView>(R.id.bottom_nav_bar)
        val navController = findNavController(R.id.nav_fragment)
        bottomNavView.setupWithNavController(navController)

        categoryviewmodel.getCategory()

        if (intent.getStringExtra(IS_LOGGED_IN).isNullOrEmpty()||intent.getStringExtra(IS_LOGGED_IN)==""){
            bottomNavView.menu.findItem(R.id.jualFragment2).setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener {
                startActivity(Intent(this, LoginActivity::class.java))
                return@OnMenuItemClickListener true
            })

            bottomNavView.menu.findItem(R.id.akunFragment2).setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener {
                startActivity(Intent(this, LoginActivity::class.java))
                return@OnMenuItemClickListener true
            })
        }
        else {
            bottomNavView.menu.findItem(R.id.jualFragment2).setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener {
                navController.navigate(R.id.jualFragment2)
                return@OnMenuItemClickListener true
            })

            bottomNavView.menu.findItem(R.id.akunFragment2).setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener {
                navController.navigate(R.id.akunFragment2)
                return@OnMenuItemClickListener true
            })
        }

        authviewmodel.userResponseData.observe(this){
            when(authviewmodel.userResponseCode){
                200 -> {
                    if (it != null) {
                        val data = GetUserData(
                            it.address!!,
                            it.city!!,
                            it.createdAt!!,
                            it.email!!,
                            it.full_name!!,
                            it.id!!,
                            it.image_url,
                            it.password!!,
                            it.phone_number.toString(),
                            it.updatedAt!!
                        )
                        datastoreviewmodel.saveUserData(data)
                        authviewmodel.userResponseData.postValue(null)
                    }
                    authviewmodel.userResponseCode = null
                }
            }
        }

        navController.addOnDestinationChangedListener { _, nd: NavDestination, _ ->
            when (nd.id){
                R.id.jualFragment2 -> {
                    bottomNavView.visibility = View.GONE
                    categoryviewmodel.ids.value?.clear()
                }
                R.id.produkPreviewFragment -> {
                    bottomNavView.visibility = View.GONE
                }
                R.id.editProfileFragment->{
                    bottomNavView.visibility = View.GONE
                }
                R.id.detailFragment3->{
                    bottomNavView.visibility = View.GONE
                }
                R.id.searchFragment->{
                    bottomNavView.visibility = View.GONE
                }
                R.id.searchResultFragment -> {
                    bottomNavView.visibility = View.GONE
                }
                R.id.infoPenawarFragment->{
                    bottomNavView.visibility = View.GONE
                }
                else -> {
                    bottomNavView.visibility = View.VISIBLE
                }
            }
        }
    }

    companion object {
        private const val IS_LOGGED_IN = "isLoggedIn"
        fun open(context: Context, isLoggedIn: String?) {
            context.startActivity(Intent(context, BottomNavActivity::class.java).apply {
                putExtra(IS_LOGGED_IN, isLoggedIn)
            })
        }
    }
}